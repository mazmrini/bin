## Examples

Looking at different examples will showcase `bin` strength.

Notice how, regardless of the technology, the mental model to set up a project
is always the same. Split in 4 steps:

1. Install `bin` to have it ready to use
2. Verify requirements with `bin req`
3. Set up the project through `bin up`
4. Run the project with `bin start|run|server`, this is where the user freedom begins

### Python projects

We recommend installing `bin` globally because `bin` dependencies might conflict with your
project dependencies.
