from fastapi import Depends, FastAPI

from app.config import AppSettings, get_app_settings

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "hello world"}


@app.get("/status")
async def status(settings: AppSettings = Depends(get_app_settings)):
    return {"status": "UP", "app_env": settings.app_env, "app_meta": settings.app_meta}
