from functools import lru_cache

from pydantic import BaseSettings


class AppSettings(BaseSettings):
    app_env: str
    app_meta: str


@lru_cache
def get_app_settings():
    return AppSettings()
