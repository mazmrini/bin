# React app

## sbin
This project uses [sbin](https://pypi.org/project/sbin/), install it through:
```
pip install sbin
```

## Local setup
Make sure you can run the project with:
```
bin req
```

Set up the project:
```
bin up
```

Run the dev server:
```
bin start|s
```

Run the tests:
```
bin test
```

Build:
```
bin build
```

Install dependencies:
```
bin install|i <dep>
bin i dev <dep>
```