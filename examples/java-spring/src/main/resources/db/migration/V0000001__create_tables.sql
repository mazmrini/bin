CREATE TABLE todos (
  id BIGSERIAL PRIMARY KEY,
  todo VARCHAR(255) NOT NULL UNIQUE
);

INSERT INTO todos (todo) VALUES ('install bin');
INSERT INTO todos (todo) VALUES ('use it');
