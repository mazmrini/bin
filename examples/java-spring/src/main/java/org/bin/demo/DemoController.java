package org.bin.demo;

import org.springframework.web.bind.annotation.*;
import lombok.AllArgsConstructor;
import java.util.List;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
public class DemoController {

    private TodoRepository todoRepo;

    private AppConfig appConfig;

    @GetMapping("/")
    public Map<String, String> getHome() {
        Map<String, String> result = new HashMap<>();
        result.put("hello", "world");

        return result;
    }

    @GetMapping("/status")
    public Map<String, String> getStatus() {
        Map<String, String> result = new HashMap<>();
        result.put("status", "UP");
        result.put("appEnv", appConfig.getAppEnv());

        return result;
    }

    @GetMapping("/todos")
    public Map<String, List<String>> getTodos() {
        Map<String, List<String>> result = new HashMap<>();
        result.put(
            "todos",
            todoRepo.findAll().stream()
                .map((todo) -> todo.getTodo())
                .collect(Collectors.toList())
        );

        return result;
    }
}
