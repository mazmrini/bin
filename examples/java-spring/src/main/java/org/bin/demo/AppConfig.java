package org.bin.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import lombok.Getter;

@Configuration
@Getter
class AppConfig {
    @Value("${app.env}")
    private String appEnv;
}
