# Java Spring App

## sbin
This project uses [sbin](https://pypi.org/project/sbin/), install it through:
```
pip install sbin
```

## Local setup
Make sure you can run the project with:
```
bin req
```

Set up the project:
```
bin up
```

Run the server:
```
bin server|s
```

Run the tests:
```
bin test|t
```
