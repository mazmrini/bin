class Emoji:
    SUCCESS = "✅"
    FAILURE = "❌"
    ERROR = FAILURE
    THINKING_FACE = "🤔"
    WARNING = "😶"
    BIN = "🗑"
    PLANE_UP = "🛫"
    PLANE_DOWN = "🛬"
    HOME = "🏠"
    NOTE = "📋"
