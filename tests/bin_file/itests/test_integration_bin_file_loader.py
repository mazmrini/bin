import unittest
from pathlib import Path

from bin.bin_file.bin_file_loader import BinFileLoader
from bin.bin_file.errors import BinFileNotFound, BinFileParsingError

TEST_CASES_DIR = Path(__file__).parent / "test_cases"


class BinFileLoaderIntegrationTests(unittest.TestCase):
    def setUp(self) -> None:
        self.expected_bin = {"name": "my bin file", "req": ["one", "two"]}

    def test__valid_simple__load__returns_expected_bin_dict(self) -> None:
        valid_simple = TEST_CASES_DIR / "valid_simple"

        actual = BinFileLoader.load(valid_simple)

        self.assertEqual(self.expected_bin, actual)

    def test__valid_custom__load__returns_expected_bin_dict(self) -> None:
        valid_custom = TEST_CASES_DIR / "valid_custom"

        actual = BinFileLoader.load(valid_custom)

        self.assertEqual(self.expected_bin, actual)

    def test__not_found__load__raises_error(self) -> None:
        not_found = TEST_CASES_DIR / "not_found"

        with self.assertRaises(BinFileNotFound) as ctx:
            BinFileLoader.load(not_found)

        self.assertIn("not_found.yaml", str(ctx.exception))

    def test__empty_bin_file__load__raises_error(self) -> None:
        empty_bin = TEST_CASES_DIR / "empty_bin"

        with self.assertRaises(BinFileParsingError) as ctx:
            BinFileLoader.load(empty_bin)

        self.assertIn("empty.yaml", str(ctx.exception))

    def test__no_config__load__raises_error(self) -> None:
        no_config = TEST_CASES_DIR / "no_config"

        with self.assertRaises(BinFileNotFound) as ctx:
            BinFileLoader.load(no_config)

        self.assertIn("bin file", str(ctx.exception))
        self.assertIn("not found", str(ctx.exception))

    def test__valid_simple__find_any_bin_file__returns_bin_file(self) -> None:
        valid_simple = TEST_CASES_DIR / "valid_simple"
        expected = valid_simple / "bin.yaml"

        actual = BinFileLoader.find_any_bin_file(valid_simple)

        self.assertEqual(expected, actual)

    def test__valid_custom__find_any_bin_file__returns_bin_config_file(self) -> None:
        valid_custom = TEST_CASES_DIR / "valid_custom"
        expected = valid_custom / ".binconfig.yml"

        actual = BinFileLoader.find_any_bin_file(valid_custom)

        self.assertEqual(expected, actual)

    def test__not_found__find_any_bin_file__returns_bin_file(self) -> None:
        not_found = TEST_CASES_DIR / "not_found"
        expected = not_found / "bin.yaml"

        actual = BinFileLoader.find_any_bin_file(not_found)

        self.assertEqual(expected, actual)

    def test__empty_bin_file__find_any_bin_file__returns_bin_config_file(self) -> None:
        empty_bin = TEST_CASES_DIR / "empty_bin"
        expected = empty_bin / ".binconfig.yaml"

        actual = BinFileLoader.find_any_bin_file(empty_bin)

        self.assertEqual(expected, actual)

    def test__no_config__find_any_bin_file__returns_None(self) -> None:
        no_config = TEST_CASES_DIR / "no_config"

        actual = BinFileLoader.find_any_bin_file(no_config)

        self.assertIsNone(actual)
