import unittest
from pathlib import Path
from typing import Any, List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions
from parameterized import parameterized

from bin.bin_file.bin_file_loader import BinFileLoader
from bin.bin_file.bin_file_resolver import BinFileResolver
from bin.bin_file.errors import BinFileNotFound, BinFileParsingError


class BinFileLoaderTests(unittest.TestCase):
    def setUp(self) -> None:
        self.load_path = mock(Path, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_valid_config_file_and_valid_bin_file__load__returns_bin_file_as_dict(
        self,
    ) -> None:
        config_content = b"bin_file: path/to/bin-custom.yaml"
        bin_path = "path/to/bin-custom.yaml"
        bin_content = b"""
        name: valid bin file
        req:
          - one
          - two
        """
        expected = {"name": "valid bin file", "req": ["one", "two"]}
        self._given_config_file_exists(config_content)
        self._given_bin_file_exists([bin_path], bin_content)

        actual = BinFileLoader.load(self.load_path)

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            ("empty", b""),
            ("invalid yaml", b"invalid: yaml: file"),
            ("not a dict", b"not a dict"),
        ]
    )
    def test__given_valid_config_file_but_invalid_bin_file__load__raises_error(
        self, _: str, invalid_bin_content: bytes
    ) -> None:
        config_content = b"bin_file: path/to/bin-custom.yaml"
        bin_file = "path/to/bin-custom.yaml"
        self._given_config_file_exists(config_content)
        bin_path = self._given_bin_file_exists([bin_file], invalid_bin_content)
        expected = f"invalid bin file '{bin_path}'"

        with self.assertRaises(BinFileParsingError) as ctx:
            BinFileLoader.load(self.load_path)

        self.assertEqual(expected, str(ctx.exception))

    def test__given_valid_config_file_but_no_existing_bin_file__load__raises_error(
        self,
    ) -> None:
        config_content = b"bin_file: path/to/bin-custom.yaml"
        bin_file = "path/to/bin-custom.yaml"
        self._given_config_file_exists(config_content)
        self._given_no_bin_file([bin_file])
        expect(self.load_path, times=1).__truediv__(bin_file).thenReturn("complete/path/to/bin-custom.yaml")
        expected = "bin file 'complete/path/to/bin-custom.yaml' not found"

        with self.assertRaises(BinFileNotFound) as ctx:
            BinFileLoader.load(self.load_path)

        self.assertEqual(expected, str(ctx.exception))

    @parameterized.expand(
        [  # type: ignore
            (b"",),
            (b"invalid: yaml: file",),
            (b"bad_key: ok bye",),
            (
                b"""
        bin_file:
          - actually a list
        """,
            ),
            (b"not a dict",),
        ]
    )
    def test__given_invalid_config_file__load__raises_error(self, invalid_config_content: bytes) -> None:
        config_path = self._given_config_file_exists(invalid_config_content)
        expected = f"invalid config file '{config_path}'"

        with self.assertRaises(BinFileParsingError) as ctx:
            BinFileLoader.load(self.load_path)

        self.assertEqual(expected, str(ctx.exception))

    def test__given_no_config_file_but_valid_bin_file__load__returns_bin_file_as_dict(
        self,
    ) -> None:
        self._given_no_config_file()
        bin_content = b"""
        name: bin
        req:
          - one
        """
        self._given_bin_file_exists(["bin.yml", "bin.yaml"], bin_content)
        expected = {"name": "bin", "req": ["one"]}

        actual = BinFileLoader.load(self.load_path)

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            (b"",),
            (b"invalid: yaml: file",),
            (b"not a dict",),
        ]
    )
    def test__given_no_config_file_but_invalid_bin_file__load__raises_error(self, invalid_bin_content: bytes) -> None:
        self._given_no_config_file()
        bin_path = self._given_bin_file_exists(["bin.yml", "bin.yaml"], invalid_bin_content)
        expected = f"invalid bin file '{bin_path}'"

        with self.assertRaises(BinFileParsingError) as ctx:
            BinFileLoader.load(self.load_path)

        self.assertEqual(expected, str(ctx.exception))

    def test__given_no_config_file_and_no_bin_file__load__raises_error(self) -> None:
        self._given_no_config_file()
        self._given_no_bin_file(["bin.yml", "bin.yaml"])
        absolute_load_path = mock(Path, strict=True)
        expect(self.load_path, times=1).absolute().thenReturn(absolute_load_path)
        expected = f"bin file in dir '{absolute_load_path}' not found"

        with self.assertRaises(BinFileNotFound) as ctx:
            BinFileLoader.load(self.load_path)

        self.assertEqual(expected, str(ctx.exception))

    def test__find_any_bin_file__prioritizes_bin_file_over_bin_config_and_returns_resolver_result(
        self,
    ) -> None:
        expected = Path("resolver/found/bin.yaml")
        expect(BinFileResolver, times=1).find_first_file(
            self.load_path, ["bin.yml", "bin.yaml", ".binconfig.yml", ".binconfig.yaml"]
        ).thenReturn(expected)

        actual = BinFileLoader.find_any_bin_file(self.load_path)

        self.assertEqual(expected, actual)

    def _given_config_file_exists(self, content: bytes) -> Any:
        config_path = mock(Path, strict=True)
        expect(BinFileResolver, times=1).find_first_file(
            self.load_path, [".binconfig.yml", ".binconfig.yaml"]
        ).thenReturn(config_path)
        expect(config_path, times=1).read_bytes().thenReturn(content)

        return config_path

    def _given_bin_file_exists(self, bin_files: List[str], content: bytes) -> Any:
        bin_path = mock(Path, strict=True)
        expect(BinFileResolver, times=1).find_first_file(self.load_path, bin_files).thenReturn(bin_path)
        expect(bin_path, times=1).read_bytes().thenReturn(content)

        return bin_path

    def _given_no_config_file(self) -> None:
        expect(BinFileResolver, times=1).find_first_file(
            self.load_path, [".binconfig.yml", ".binconfig.yaml"]
        ).thenReturn(None)

    def _given_no_bin_file(self, bin_files: List[str]) -> None:
        expect(BinFileResolver, times=1).find_first_file(self.load_path, bin_files).thenReturn(None)
