import unittest
from pathlib import Path

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions

from bin.bin_file.bin_file_resolver import BinFileResolver


class BinFileResolverTests(unittest.TestCase):
    def setUp(self) -> None:
        self.parent_path = mock(Path, strict=True)
        self.valid_file_path = mock(Path, strict=True)
        self.invalid_file_path = mock(Path, strict=True)
        expect(self.invalid_file_path).is_file().thenReturn(False)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_first_file_exists__find_first_file__returns_first_path(
        self,
    ) -> None:
        expect(self.parent_path, times=1).__truediv__("first").thenReturn(self.valid_file_path)
        expect(self.valid_file_path, times=1).is_file().thenReturn(True)

        actual = BinFileResolver.find_first_file(self.parent_path, ["first", "second.json", "third.yaml"])

        self.assertEqual(self.valid_file_path, actual)

    def test__given_last_file_exists__find_first_file__returns_last_path(self) -> None:
        expect(self.parent_path, times=1).__truediv__("first").thenReturn(self.invalid_file_path)
        expect(self.parent_path, times=1).__truediv__("second.json").thenReturn(self.invalid_file_path)
        expect(self.parent_path, times=1).__truediv__("third.yaml").thenReturn(self.valid_file_path)
        expect(self.valid_file_path, times=1).is_file().thenReturn(True)

        actual = BinFileResolver.find_first_file(self.parent_path, ["first", "second.json", "third.yaml"])

        self.assertEqual(self.valid_file_path, actual)

    def test__given_no_files_exist__find_first_file__returns_None(self) -> None:
        expect(self.parent_path, times=1).__truediv__("first").thenReturn(self.invalid_file_path)
        expect(self.parent_path, times=1).__truediv__("second.json").thenReturn(self.invalid_file_path)
        expect(self.parent_path, times=1).__truediv__("third.yaml").thenReturn(self.invalid_file_path)

        actual = BinFileResolver.find_first_file(self.parent_path, ["first", "second.json", "third.yaml"])

        self.assertIsNone(actual)

    def test__given_no_files__find_first_file__returns_None(self) -> None:
        actual = BinFileResolver.find_first_file(self.parent_path, [])

        self.assertIsNone(actual)
        verifyZeroInteractions(self.parent_path)
