import unittest

import yaml
from parameterized import parameterized

from bin.bin_file.dtos.bin_file_dto import BinFileDto, ErrorMsg
from bin.commands.errors import CommandParseError
from bin.commands.internal.factory import InternalCommandFactory
from bin.commands.internal.log_title_command import LogTitleCommand
from bin.custom_commands.basic_command import BasicCommand
from bin.custom_commands.command_tree import CommandTree
from bin.env.log_env_command import LogEnvCommand
from bin.requirements.requirements_command import RequirementsCommand
from bin.up.down_command import DownCommand
from bin.up.simple_up_subcommand import SimpleUpSubcommand
from bin.up.up_command import UpCommand
from bin.version.version_command import VersionCommand
from bin.virtualenv.activate_venv_command import ActivateVenvCommand
from bin.virtualenv.create_venv_command import CreateVenvCommand
from bin.virtualenv.remove_venv_command import RemoveVenvCommand
from bin.virtualenv.venv import Venv


class BinFileDtoTests(unittest.TestCase):
    def test__minimal_file__parse_obj_explicitly__returns_dto(self) -> None:
        data = yaml.safe_load("name: minimal")

        actual = BinFileDto.parse_obj_explicitly(data)

        self.assertIsNotNone(actual)

    def test__given_venv__to_command__returns_command_with_venv_setup(self) -> None:
        data = yaml.safe_load(
            """
        name: minimal with venv
        env:
          ONE: first_var
        venv:
          create: echo create
          exists: echo exists
          activate: echo activate
          remove: echo remove
        commands:
          one: echo one
          two:
            run: echo two
            alias: twotwo
          three:
            up:
            - echo three up
            subcommands:
              inner: echo three inner
        """
        )
        venv = Venv(
            create_cmd="echo create",
            activate_cmd="echo activate",
            exists_cmd="echo exists",
            remove_cmd="echo remove",
        )
        req = InternalCommandFactory.with_setup([LogTitleCommand.requirements()], RequirementsCommand(requirements=[]))
        custom_commands_setup = [
            InternalCommandFactory.mute_logs(),
            CreateVenvCommand.from_venv(venv),
            ActivateVenvCommand.from_venv(venv),
            InternalCommandFactory.unmute_logs(),
        ]
        env = {"ONE": "first_var"}
        expected = InternalCommandFactory.wrap_with_env(
            env,
            CommandTree(
                subcommands_tree={
                    "--version": VersionCommand(),
                    "env": LogEnvCommand.from_env(env),
                    "req": req,
                    "requirements": req,
                    "venv": CommandTree(
                        subcommands_tree={
                            "create": CreateVenvCommand.from_venv(venv),
                            "activate": ActivateVenvCommand.from_venv(venv),
                            "remove": RemoveVenvCommand.from_venv(venv),
                            "rm": RemoveVenvCommand.from_venv(venv),
                        }
                    ),
                    "up": InternalCommandFactory.with_setup(
                        [
                            LogTitleCommand.venv(),
                            CreateVenvCommand.from_venv(venv),
                            ActivateVenvCommand.from_venv(venv),
                            LogTitleCommand.up(),
                        ],
                        UpCommand(subcommands=[]),
                    ),
                    "down": InternalCommandFactory.with_setup(
                        [
                            LogTitleCommand.venv(),
                            CreateVenvCommand.from_venv(venv),
                            ActivateVenvCommand.from_venv(venv),
                            LogTitleCommand.down(),
                        ],
                        DownCommand(subcommands=[]),
                    ),
                    "one": InternalCommandFactory.with_setup(
                        custom_commands_setup,
                        BasicCommand.no_help("echo one"),
                    ),
                    "two": InternalCommandFactory.with_setup(
                        custom_commands_setup,
                        BasicCommand.no_help("echo two"),
                    ),
                    "twotwo": InternalCommandFactory.with_setup(
                        custom_commands_setup,
                        BasicCommand.no_help("echo two"),
                    ),
                    "three": InternalCommandFactory.with_setup(
                        custom_commands_setup,
                        CommandTree(
                            subcommands_tree={
                                "up": InternalCommandFactory.with_setup(
                                    [LogTitleCommand.up()],
                                    UpCommand(subcommands=[SimpleUpSubcommand.from_str("echo three up")]),
                                ),
                                "down": InternalCommandFactory.with_setup(
                                    [LogTitleCommand.down()],
                                    DownCommand(subcommands=[]),
                                ),
                                "inner": BasicCommand.no_help("echo three inner"),
                            }
                        ),
                    ),
                }
            ),
        )

        actual = BinFileDto.parse_obj_explicitly(data).to_command()

        self.assertEqual(expected, actual)

    def test__given_no_venv_and__to_command__returns_command_without_venv_setup(
        self,
    ) -> None:
        data = yaml.safe_load(
            """
        name: minimal with venv
        env:
          ONE: first_var
        commands:
          one: echo one
          two:
            run: echo two
            alias: twotwo
          three:
            subcommands:
              inner: echo three inner

        """
        )
        req = InternalCommandFactory.with_setup([LogTitleCommand.requirements()], RequirementsCommand(requirements=[]))
        env = {"ONE": "first_var"}
        expected = InternalCommandFactory.wrap_with_env(
            env,
            CommandTree(
                subcommands_tree={
                    "--version": VersionCommand(),
                    "env": LogEnvCommand.from_env(env),
                    "req": req,
                    "requirements": req,
                    "up": InternalCommandFactory.with_setup(
                        [LogTitleCommand.up()],
                        UpCommand(subcommands=[]),
                    ),
                    "down": InternalCommandFactory.with_setup(
                        [LogTitleCommand.down()],
                        DownCommand(subcommands=[]),
                    ),
                    "one": BasicCommand.no_help("echo one"),
                    "two": BasicCommand.no_help("echo two"),
                    "twotwo": BasicCommand.no_help("echo two"),
                    "three": CommandTree(subcommands_tree={"inner": BasicCommand.no_help("echo three inner")}),
                }
            ),
        )

        actual = BinFileDto.parse_obj_explicitly(data).to_command()

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            (
                "all empty but extra keyword",
                """
            other_extra: something lese
            extra: im not valid
            """,
                (
                    f"{ErrorMsg.INVALID_FILE}\n"
                    f"  {ErrorMsg.EXTRA_TOP_LEVEL_KEYWORDS}: extra, other_extra\n"
                    f"  env         : {ErrorMsg.EMPTY_ENV}\n"
                    f"  requirements: {ErrorMsg.EMPTY_REQ}\n"
                    f"  venv        : {ErrorMsg.EMPTY_VENV}\n"
                    f"  up          : {ErrorMsg.EMPTY_UP}\n"
                    f"  commands    : {ErrorMsg.EMPTY_COMMANDS}"
                ),
            ),
            (
                "all valid but extra keyword",
                """
            extra: im not valid  # extra keyword
            requirements:
              - python: ^1.2.3
            env:
              ONE: 1
            venv:
              create: echo create
              exists: echo exists
              activate: echo activate
              remove: echo remove
            up:
              - echo up
            commands:
              one: echo one
            other_extra: other  # extra keyword
            """,
                (
                    f"{ErrorMsg.INVALID_FILE}\n"
                    f"  {ErrorMsg.EXTRA_TOP_LEVEL_KEYWORDS}: extra, other_extra\n"
                    f"  env         : {ErrorMsg.VALID}\n"
                    f"  requirements: {ErrorMsg.VALID}\n"
                    f"  venv        : {ErrorMsg.VALID}\n"
                    f"  up          : {ErrorMsg.VALID}\n"
                    f"  commands    : {ErrorMsg.VALID}"
                ),
            ),
            (
                "all not right type",
                """
            name: ok
            description: 133
            requirements: text
            env: text
            venv: text
            up: text
            commands: text
            """,
                (
                    f"{ErrorMsg.INVALID_FILE}\n"
                    f"  env         : {ErrorMsg.custom_error('must be a dictionary')}\n"
                    f"  requirements: {ErrorMsg.MUST_BE_A_LIST}\n"
                    f"  venv        : {ErrorMsg.MUST_BE_A_FLAT_DICT}\n"
                    f"  up          : {ErrorMsg.MUST_BE_A_LIST}\n"
                    f"  commands    : {ErrorMsg.MUST_BE_A_DICT}"
                ),
            ),
            (
                "errors in all",
                """
            requirements:
              - python: ^1.3.0
              - invalid  # [1] fails
              - python: ^2.6.0
              - invalid  # [3] fails
              - why: do i do this  # [4] fails
              - python: ^3.5.0
            env:
              VALID: super
              INVALID_VALUE:
                - should not
                - be a list
            venv:
              create: echo create
              invalid: this should not exist
            up:
              - echo this works
              - name: this will fail  # [1] fails
                up: echo up
                invalid_key: rip
              - echo this one works
              - echo this works too
              - invalid: this is not allowed  # [4] fails
              - echo im valid
              - rip: still not allowed  # [6] fails
            commands:
              one: echo one
              two:
                run: echo run
                alias: one  # fails
            """,
                (
                    f"{ErrorMsg.INVALID_FILE}\n"
                    f"  env         : {ErrorMsg.custom_error('values must be a string or number')}\n"
                    f"  requirements: {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.list_valid(0)}\n"
                    f"    {ErrorMsg.list_invalid(1)}\n"
                    f"    {ErrorMsg.list_valid(2)}\n"
                    f"    {ErrorMsg.list_invalid(3)}\n"
                    f"    {ErrorMsg.list_invalid(4)}\n"
                    f"    {ErrorMsg.list_valid(5)}\n"
                    f"  venv        : {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.INVALID_VENV_MSG}\n"
                    f"  up          : {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.list_valid(0)}\n"
                    f"    {ErrorMsg.list_invalid(1)}\n"
                    f"    {ErrorMsg.list_valid(2)}\n"
                    f"    {ErrorMsg.list_valid(3)}\n"
                    f"    {ErrorMsg.list_invalid(4)}\n"
                    f"    {ErrorMsg.list_valid(5)}\n"
                    f"    {ErrorMsg.list_invalid(6)}\n"
                    f"  commands    : {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.INVALID_COMMANDS_MSG}"
                ),
            ),
            (
                "some are fine other are failures",
                """
            requirements:
              - python: ^1.3.0
            env:
              VALID: super
              WORKS: great
            venv: text  # fails
            up:
              - echo this works
              - name: this will fail  # [1] fails
                up: echo up
                invalid_key: rip
              - invalid: this is not allowed  # [2] fails
              - echo im valid
              - echo valid too
            commands:
              one: echo im ok
              req: clashes with bin command names  # fails
              two: echo im ok too
            """,
                (
                    f"{ErrorMsg.INVALID_FILE}\n"
                    f"  env         : {ErrorMsg.VALID}\n"
                    f"  requirements: {ErrorMsg.VALID}\n"
                    f"  venv        : {ErrorMsg.MUST_BE_A_FLAT_DICT}\n"
                    f"  up          : {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.list_valid(0)}\n"
                    f"    {ErrorMsg.list_invalid(1)}\n"
                    f"    {ErrorMsg.list_invalid(2)}\n"
                    f"    {ErrorMsg.list_valid(3)}\n"
                    f"    {ErrorMsg.list_valid(4)}\n"
                    f"  commands    : {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.COMMANDS_USING_RESERVED_KEYWORDS}"
                ),
            ),
            (
                "various failures",
                """
            requirements:
              - other: i dont exist  # [0] fails
              - python: ^1.2.3
            # env: empty
            venv:
              create: im fine
              exists: im fine too
              activate: very nice
              remove: same here
            up:
              why: am i a dictionary
            commands:
              one: echo im ok
              req: clashes with command names
              two: echo im ok too
            """,
                (
                    f"{ErrorMsg.INVALID_FILE}\n"
                    f"  env         : {ErrorMsg.EMPTY_ENV}\n"
                    f"  requirements: {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.list_invalid(0)}\n"
                    f"    {ErrorMsg.list_valid(1)}\n"
                    f"  venv        : {ErrorMsg.VALID}\n"
                    f"  up          : {ErrorMsg.MUST_BE_A_LIST}\n"
                    f"  commands    : {ErrorMsg.ERROR}\n"
                    f"    {ErrorMsg.COMMANDS_USING_RESERVED_KEYWORDS}"
                ),
            ),
        ]
    )
    def test__given_invalid_bin_file_dto__parse_obj_explicitly__raises_error(
        self, _: str, yaml_data: str, expected_msg: str
    ) -> None:
        data = yaml.safe_load(yaml_data)

        with self.assertRaises(CommandParseError) as ctx:
            BinFileDto.parse_obj_explicitly(data)

        actual = str(ctx.exception)

        self.assertEqual(expected_msg, actual)

    @parameterized.expand(
        [  # type: ignore
            ("req",),
            ("requirements",),
            ("venv",),
            ("up",),
            ("down",),
            ("env",),
            ("update-bin",),
            ("--version",),
        ]
    )
    def test__given_custom_commands_clashes_with_reserved_ones__parse_obj_explicitly__raises_error(
        self, cmd: str
    ) -> None:
        yaml_data = f"""
        name: custom commands clashes
        commands:
          {cmd}: run clashes
          valid: run valid
        """
        data = yaml.safe_load(yaml_data)
        expected = (
            f"{ErrorMsg.INVALID_FILE}\n"
            f"  env         : {ErrorMsg.EMPTY_ENV}\n"
            f"  requirements: {ErrorMsg.EMPTY_REQ}\n"
            f"  venv        : {ErrorMsg.EMPTY_VENV}\n"
            f"  up          : {ErrorMsg.EMPTY_UP}\n"
            f"  commands    : {ErrorMsg.ERROR}\n"
            f"    {ErrorMsg.COMMANDS_USING_RESERVED_KEYWORDS}"
        )

        with self.assertRaises(CommandParseError) as ctx:
            BinFileDto.parse_obj_explicitly(data)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)
