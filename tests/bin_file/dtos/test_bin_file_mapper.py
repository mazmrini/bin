import unittest
from pathlib import Path
from typing import Dict

import yaml
from mockito import expect, unstub, verifyNoUnwantedInteractions

from bin.bin_file.bin_file_loader import BinFileLoader
from bin.bin_file.dtos.bin_file_mapper import BinFileMapper
from bin.bin_file.errors import BinFileNotFound
from bin.commands.command import Command
from bin.commands.errors import CommandParseError
from bin.commands.internal.factory import InternalCommandFactory
from bin.commands.internal.log_title_command import LogTitleCommand
from bin.custom_commands.basic_command import BasicCommand
from bin.custom_commands.command_tree import CommandTree
from bin.env.log_env_command import LogEnvCommand
from bin.requirements.requirements_command import RequirementsCommand
from bin.requirements.shell_requirement import ShellRequirement
from bin.up.conditional_up_subcommand import ConditionalUpSubcommand
from bin.up.down_command import DownCommand
from bin.up.simple_up_subcommand import SimpleUpSubcommand
from bin.up.up_command import UpCommand
from bin.version.version_command import VersionCommand
from bin.virtualenv.activate_venv_command import ActivateVenvCommand
from bin.virtualenv.create_venv_command import CreateVenvCommand
from bin.virtualenv.remove_venv_command import RemoveVenvCommand
from bin.virtualenv.venv import Venv


class BinCommandMapperTests(unittest.TestCase):
    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_example_file_content__to_command__returns_command(self) -> None:
        content = BinFileMapper.example_file_content()
        data = yaml.safe_load(content)
        path = Path("example_file")
        expect(BinFileLoader, times=1).load(path).thenReturn(data)
        expected = self._expected_example_command()

        actual = BinFileMapper.to_command(path)

        self.assertEqual(expected, actual)

    def test__given_minimal_dto__to_command__returns_command(self) -> None:
        data = yaml.safe_load("""name: bin""")
        path = Path("minimal_path")
        expect(BinFileLoader, times=1).load(path).thenReturn(data)
        empty_req = InternalCommandFactory.with_setup(
            [LogTitleCommand.requirements()], RequirementsCommand(requirements=[])
        )
        expected = CommandTree(
            subcommands_tree={
                "--version": VersionCommand(),
                "env": LogEnvCommand.from_env({}),
                "req": empty_req,
                "requirements": empty_req,
                "up": InternalCommandFactory.with_setup(
                    [LogTitleCommand.up()],
                    UpCommand(subcommands=[]),
                ),
                "down": InternalCommandFactory.with_setup(
                    [LogTitleCommand.down()],
                    DownCommand(subcommands=[]),
                ),
            }
        )

        actual = BinFileMapper.to_command(path)

        self.assertEqual(expected, actual)

    def test__given_bin_file_loader_raises_error__to_command__raises_the_same_error(
        self,
    ) -> None:
        failing_path = Path("failed")
        expected = BinFileNotFound("missing file")
        expect(BinFileLoader, times=1).load(failing_path).thenRaise(expected)

        with self.assertRaises(BinFileNotFound) as ctx:
            BinFileMapper.to_command(failing_path)

        actual = ctx.exception

        self.assertEqual(expected, actual)

    def test__given_bin_file_dto_correctness_check_fails__to_command__returns_command(
        self,
    ) -> None:
        content = """
        name: venv is incorrect
        venv:
          invalid: not a valid keyword
        """
        data = yaml.safe_load(content)
        path = Path("example_file")
        expect(BinFileLoader, times=1).load(path).thenReturn(data)

        with self.assertRaises(CommandParseError) as ctx:
            BinFileMapper.to_command(path)

        actual = str(ctx.exception)

        self.assertIn("invalid bin file, details:", actual)
        self.assertIn("env         :", actual)
        self.assertIn("requirements:", actual)
        self.assertIn("venv        :", actual)
        self.assertIn("up          :", actual)
        self.assertIn("commands    :", actual)

    def test__given_bin_file_loader_finds_a_bin_file__contains_bin_file__returns_true(
        self,
    ) -> None:
        path = Path("my_dir")
        expect(BinFileLoader, times=1).find_any_bin_file(path).thenReturn(path / "bin.yml")

        actual = BinFileMapper.contains_bin_file(path)

        self.assertTrue(actual)

    def test__given_bin_file_loader_returns_none__contains_bin_file__returns_false(
        self,
    ) -> None:
        path = Path("my_dir")
        expect(BinFileLoader, times=1).find_any_bin_file(path).thenReturn(None)

        actual = BinFileMapper.contains_bin_file(path)

        self.assertFalse(actual)

    def _expected_example_command(self) -> Command:
        req_cmd = self._req_command()
        req_cmd_with_title = InternalCommandFactory.with_setup([LogTitleCommand.requirements()], req_cmd)
        env = {
            "APP_ENV": "dev",
            "DB_URL": "postgresql://mydb.local:5432/database",
            "DB_PASSWORD": "imsupersecure123",
        }
        return InternalCommandFactory.wrap_with_env(
            env,
            CommandTree(
                subcommands_tree={
                    "--version": VersionCommand(),
                    "env": LogEnvCommand.from_env(env),
                    "req": req_cmd_with_title,
                    "requirements": req_cmd_with_title,
                    "up": self._up_command(),
                    "down": self._down_command(),
                    "venv": self._venv_command(),
                    **self._custom_commands(),
                }
            ),
        )

    def _req_command(self) -> Command:
        return RequirementsCommand(
            requirements=[
                ShellRequirement(
                    name="Docker",
                    met_if_cmd="echo docker --version",
                    help="Download docker @ https://docker.com",
                )
            ]
        )

    def _venv_command(self) -> Command:
        venv = self._venv()

        return CommandTree(
            subcommands_tree={
                "create": CreateVenvCommand.from_venv(venv),
                "activate": ActivateVenvCommand.from_venv(venv),
                "remove": RemoveVenvCommand.from_venv(venv),
                "rm": RemoveVenvCommand.from_venv(venv),
            }
        )

    def _venv(self) -> Venv:
        return Venv(
            create_cmd="echo create venv",
            activate_cmd="echo activate venv",
            exists_cmd="echo test -e venv",
            remove_cmd="echo rm venv",
        )

    def _up_command(self) -> Command:
        venv = self._venv()

        return InternalCommandFactory.with_setup(
            [
                LogTitleCommand.venv(),
                CreateVenvCommand.from_venv(venv),
                ActivateVenvCommand.from_venv(venv),
                LogTitleCommand.up(),
            ],
            UpCommand(
                subcommands=[
                    SimpleUpSubcommand(name="echo always run", run_cmd="echo always run"),
                    ConditionalUpSubcommand(
                        name="docker-compose",
                        run_cmd="echo docker-compose up -d",
                        unless_cmd="echo docker up unless",
                    ),
                ]
            ),
        )

    def _down_command(self) -> Command:
        venv = self._venv()

        return InternalCommandFactory.with_setup(
            [
                LogTitleCommand.venv(),
                CreateVenvCommand.from_venv(venv),
                ActivateVenvCommand.from_venv(venv),
                LogTitleCommand.down(),
            ],
            DownCommand(
                subcommands=[
                    ConditionalUpSubcommand(
                        name="docker-compose",
                        run_cmd="echo docker-compose down",
                        unless_cmd="echo docker down unless",
                    ),
                ]
            ),
        )

    def _custom_commands(self) -> Dict[str, Command]:
        venv = self._venv()

        def with_setup(main: Command) -> Command:  # noqa
            return InternalCommandFactory.with_setup(
                [
                    InternalCommandFactory.mute_logs(),
                    CreateVenvCommand.from_venv(venv),
                    ActivateVenvCommand.from_venv(venv),
                    InternalCommandFactory.unmute_logs(),
                ],
                main,
            )

        test_cmd = with_setup(
            InternalCommandFactory.wrap_with_env({"APP_ENV": "test"}, BasicCommand.no_help("echo run tests"))
        )
        lint_cmd = with_setup(
            CommandTree(
                self_cmd=BasicCommand.no_help("echo lint all"),
                subcommands_tree={
                    "frontend": BasicCommand.no_help("echo frontend lint"),
                    "backend": BasicCommand.no_help("echo backend lint"),
                },
            )
        )
        return {
            "server": with_setup(BasicCommand.no_help("echo run server")),
            "test": test_cmd,
            "t": test_cmd,
            "lint": lint_cmd,
            "format": lint_cmd,
            "style": lint_cmd,
            "check": lint_cmd,
        }
