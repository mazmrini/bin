import unittest
from pathlib import Path
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.bin_file.bin_file_loader import BinFileLoader
from bin.bin_file.dtos.bin_file_mapper import BinFileMapper
from bin.bin_file.init_command import InitCommand
from bin.commands.errors import CommandFailedError
from bin.process.emoji import Emoji
from bin.process.process import Process
from tests.test_cmd_utils import TestHelpCommand


class InitCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.process = mock(Process, strict=True)
        self.current_working_dir = mock(Path, strict=True)

        self.subject = InitCommand(current_working_dir=self.current_working_dir)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_no_bin_config__run__creates_it_and_logs_success(self) -> None:
        bin_path = mock(Path, strict=True)
        expect(BinFileLoader, times=1).find_any_bin_file(self.current_working_dir).thenReturn(None)
        expect(self.current_working_dir, times=1).is_dir().thenReturn(True)
        expect(self.current_working_dir, times=1).__truediv__("bin.yml").thenReturn(bin_path)
        expect(bin_path, times=1).write_text(BinFileMapper.example_file_content()).thenReturn(10)
        expect(self.process, times=1).log_success(f"{Emoji.SUCCESS}  {bin_path} initialized!")

        actual = self.subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    def test__given_current_working_dir_is_not_a_dir__run__logs_and_raises_error(
        self,
    ) -> None:
        expect(self.current_working_dir, times=1).is_dir().thenReturn(False)
        expected = CommandFailedError.failed_with_reason(
            "init",
            f"unable to initialize as '{self.current_working_dir}' is not a directory",
        )

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    def test__given_contains_bin_config__run__logs_and_raises_error(self) -> None:
        expect(self.current_working_dir, times=1).is_dir().thenReturn(True)
        found_bin_config = Path("found/bin.yaml")
        expect(BinFileLoader, times=1).find_any_bin_file(self.current_working_dir).thenReturn(found_bin_config)
        expected = CommandFailedError.failed_with_reason("init", "bin file already exists 'found/bin.yaml'")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_args__run__raises_error(self, _: str, args: List[str]) -> None:
        expected = CommandFailedError.must_run_without_args("init")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_help__run__logs_help(self) -> None:
        TestHelpCommand.assert_help_runs(self.subject, self.process)
