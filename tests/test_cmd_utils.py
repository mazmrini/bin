from typing import Any

from mockito import ANY, expect, verify

from bin.commands.command import Command
from bin.process.process import Process


class TestHelpCommand:
    @staticmethod
    def assert_help_runs(subject: Command, process: Process, expected: Any = ANY) -> None:
        expect(process, times=1).log_help(expected)

        actual = subject.run(process, ["--help"])

        assert actual == process
        verify(process, times=0).run(ANY, ANY)
