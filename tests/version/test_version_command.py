import unittest
from typing import List

from mockito import ANY, expect, mock, unstub, verify, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.errors import CommandFailedError
from bin.process.process import Process
from bin.version.version_command import VersionCommand
from tests.process.factories import ProcessResultFactory
from tests.test_cmd_utils import TestHelpCommand


class VersionCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

        self.subject = VersionCommand()

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            (
                "partial output",
                ("Name: sbin\n" "Version: 0.0.3\n" "Author: Me myself and I\n" "License: MIT"),
                "sbin 0.0.3",
            ),
            (
                "partial output different name and version",
                ("Name: weird-name\n" "Version: 12.1.4\n" "Author: Me myself and I\n" "License: MIT"),
                "sbin 12.1.4",
            ),
            (
                "different partial output",
                ("Author: Me myself and I\n" "Name: sbin\n" "Version: 1.1.1\n" "License: MIT"),
                "sbin 1.1.1",
            ),
            ("only name and version", ("Name: sbin\n" "Version: 0.0.0"), "sbin 0.0.0"),
        ]
    )
    def test__given_command_worked__run__logs_version(self, _: str, cmd_stdout: str, expected_log: str) -> None:
        expect(self.process, times=1).run("pip show sbin", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded(cmd_stdout)
        )
        expect(self.process, times=1).log(expected_log)

        actual = self.subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    def test__given_pip_command_succeeds_but_does_not_contain_version__run__raises_error(
        self,
    ) -> None:
        expect(self.process, times=1).run("pip show sbin", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded("no version in here")
        )
        expected = CommandFailedError.failed_with_reason("version", "unable to find version")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verify(self.process, times=0).log(ANY)

    def test__given_pip_command_fails__run__raises_error(self) -> None:
        expect(self.process, times=1).run("pip show sbin", capture_output=True).thenReturn(
            ProcessResultFactory.failed()
        )
        expected = CommandFailedError.command_failed("version")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verify(self.process, times=0).log(ANY)

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_args__run__raises_error(self, _: str, args: List[str]) -> None:
        expected = CommandFailedError.must_run_without_args("version")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_help__run__logs_help(self) -> None:
        TestHelpCommand.assert_help_runs(self.subject, self.process)
