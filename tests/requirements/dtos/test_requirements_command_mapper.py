import unittest
from typing import List

import yaml
from pydantic import BaseModel

from bin.commands.command import Command
from bin.requirements.dtos import RequirementDto
from bin.requirements.dtos.requirements_command_mapper import RequirementsCommandMapper
from bin.requirements.requirements_command import RequirementsCommand
from bin.requirements.sem_ver_requirement import SemVerRequirement
from bin.requirements.shell_requirement import ShellRequirement


class TestModel(BaseModel):
    requirements: List[RequirementDto]


class RequirementsCommandMapperTests(unittest.TestCase):
    def test__given_dtos_as_str__to_command__returns_expected_command(self) -> None:
        data = yaml.safe_load(self._as_yaml())

        actual = RequirementsCommandMapper.to_command(TestModel.parse_obj(data).requirements)

        self.assertEqual(self._expected(), actual)

    def _as_yaml(self) -> str:
        return """
            requirements:
            - python: ">3.0.0,<5.0.0"
            - name: shell 1
              met_if: shell 1 cmd
            - name: shell 2
              met_if: shell 2 cmd
              help: help 2
            - name: semver 3
              run: semver 3 cmd
              version: ">3.3.3"
            - name: semver 4
              run: semver 4 cmd
              version: ">=4.4.4,<10.0.0"
              help: help 4
        """

    def _expected(self) -> Command:
        return RequirementsCommand(
            requirements=[
                SemVerRequirement(
                    name="Python >3.0.0,<5.0.0",
                    run_cmd="python3 --version",
                    version=">3.0.0,<5.0.0",
                    help="Install Python @ https://www.python.org/downloads",
                ),
                ShellRequirement(name="shell 1", met_if_cmd="shell 1 cmd"),
                ShellRequirement(name="shell 2", met_if_cmd="shell 2 cmd", help="help 2"),
                SemVerRequirement(name="semver 3", run_cmd="semver 3 cmd", version=">3.3.3"),
                SemVerRequirement(
                    name="semver 4",
                    run_cmd="semver 4 cmd",
                    version=">=4.4.4,<10.0.0",
                    help="help 4",
                ),
            ]
        )
