import unittest

import yaml
from pydantic import ValidationError

from bin.requirements.dtos.sem_ver_requirement_dto import SemVerRequirementDto
from tests.pydantic_utils import PydanticErrorUtils


class SemVerRequirementDtoTests(unittest.TestCase):
    def test__given_invalid_sem_ver_req_dto__parse__raises_error(self) -> None:
        data = """
        name: ""
        run: ""
        version: "invalid"
        help: ""
        extra: field
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty("name"),
            PydanticErrorUtils.empty_cmd("run"),
            PydanticErrorUtils.invalid_sem_ver("version"),
            PydanticErrorUtils.empty("help"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            SemVerRequirementDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
