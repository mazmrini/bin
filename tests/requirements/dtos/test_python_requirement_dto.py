import unittest

import yaml
from pydantic import ValidationError

from bin.requirements.dtos.python_requirement_dto import PythonRequirementDto
from tests.pydantic_utils import PydanticErrorUtils


class PythonRequirementDtoTests(unittest.TestCase):
    def test__given_invalid_python_req_dto__parse__raises_error(self) -> None:
        data = """
        python: {invalid}
        extra: field
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.invalid_sem_ver("python"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            PythonRequirementDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
