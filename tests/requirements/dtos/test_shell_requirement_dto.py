import unittest

import yaml
from pydantic import ValidationError

from bin.requirements.dtos.shell_requirement_dto import ShellRequirementDto
from tests.pydantic_utils import PydanticErrorUtils


class ShellRequirementDtoTests(unittest.TestCase):
    def test__given_invalid_shell_req_dto__parse__raises_error(self) -> None:
        data = """
        name: ""
        met_if: ""
        help: ""
        extra: field
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty("name"),
            PydanticErrorUtils.empty_cmd("met_if"),
            PydanticErrorUtils.empty("help"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            ShellRequirementDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
