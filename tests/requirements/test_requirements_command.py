import unittest
from typing import Any, List

from mockito import ANY, expect, mock, unstub, verify, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.errors import CommandFailedError
from bin.process.emoji import Emoji
from bin.process.process import Process
from bin.requirements.requirement import Requirement
from bin.requirements.requirements_command import RequirementsCommand
from tests.test_cmd_utils import TestHelpCommand


class RequirementsCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_only_successful_requirements__run__outputs_expected_message(
        self,
    ) -> None:
        reqs = [
            self._requirement(1, True),
            self._requirement_with_help(2, True),
            self._requirement(3, True),
        ]
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  1/3  name_1")
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  2/3  name_2")
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  3/3  name_3")
        expect(self.process, times=1).log_success("All requirements are met")
        subject = RequirementsCommand(requirements=reqs)

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    def test__given_no_requirements__run__is_noop_but_warns_the_user(self) -> None:
        subject = RequirementsCommand(requirements=[])
        expect(self.process, times=1).log_warning(f"{Emoji.WARNING}  No requirements are defined")

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)
        verify(self.process, times=0).run(ANY(str))

    def test__given_only_failed_requirements__run__runs_all_outputs_expected_message_then_raises_error(
        self,
    ) -> None:
        reqs = [
            self._requirement(1, False),
            self._requirement_with_help(2, False),
            self._requirement(3, False),
        ]
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  1/3  name_1")
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  2/3  name_2\n   help_2")
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  3/3  name_3")
        expect(self.process, times=1).log_error("Unmet requirements")
        subject = RequirementsCommand(requirements=reqs)
        expected = CommandFailedError.command_failed("requirements")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    def test__given_met_and_unmet_requirements__run__runs_all_outputs_expected_messages_then_raises_error(
        self,
    ) -> None:
        reqs = [
            self._requirement(1, True),
            self._requirement(2, False),
            self._requirement_with_help(3, True),
            self._requirement_with_help(4, False),
        ]
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  1/4  name_1")
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  2/4  name_2")
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  3/4  name_3")
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  4/4  name_4\n   help_4")
        expect(self.process, times=1).log_error("Unmet requirements")
        subject = RequirementsCommand(requirements=reqs)
        expected = CommandFailedError.command_failed("requirements")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_args__run__raises_error(self, _: str, args: List[str]) -> None:
        reqs = [mock(Requirement)]
        subject = RequirementsCommand(requirements=reqs)
        expected = CommandFailedError.must_run_without_args("requirements")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_help__run__logs_help(self) -> None:
        subject = RequirementsCommand(requirements=[mock(Requirement), mock(Requirement)])

        TestHelpCommand.assert_help_runs(subject, self.process)

    def _requirement(self, i: int, is_met: bool) -> Any:
        req = mock(Requirement, strict=True)
        expect(req, times=1).get_name().thenReturn(f"name_{i}")
        expect(req, times=1).is_met(self.process).thenReturn(is_met)
        expect(req).get_help().thenReturn(None)

        return req

    def _requirement_with_help(self, i: int, is_met: bool) -> Any:
        req = self._requirement(i, is_met)
        expect(req).get_help().thenReturn(f"help_{i}")

        return req
