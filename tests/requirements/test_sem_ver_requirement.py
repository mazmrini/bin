import unittest
from typing import Dict

from mockito import ANY, expect, mock, unstub, verifyNoUnwantedInteractions
from parameterized import parameterized
from pydantic import ValidationError

from bin.process.process import Process
from bin.requirements.sem_ver_requirement import SemVerRequirement
from tests.process.factories import ProcessResultFactory
from tests.pydantic_utils import PydanticErrorUtils


class SemVerRequirementTests(unittest.TestCase):
    NAME = "SemVer name"
    RUN = "my_command"
    VERSION_SPEC = ">2.1.0,<20.43.0"

    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

        self.subject = SemVerRequirement(name=self.NAME, run_cmd=self.RUN, version=self.VERSION_SPEC)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            ("Python 3.5.0\n",),
            ("Poetry (version 2.2.0)\n",),
            ("openjdk 8.11.2 2020-04-12\nAdoptOpenJDK-8.11.2+9 (build 8.11.2+)\n",),
            ("Docker version 20.10.11 build dea9396\n",),
            ("docker-compose version 2.29.2 build 48a98sdn\n",),
            ("git version 2.35.1.windows2\n",),
            ("pip 20.2.3 from windows/path (python 3.9)\n",),
            ("grep (BSD grep, GNU compatible) 2.6.0-FreeBSD",),
            ("curl 7.79.1 (x86_64-apple) libcurl/291.79.1 LibreSL/3.3.5",),
            ("GNU bash, version 3.2.57(1)-release (x86_64-apple)",),
        ]
    )
    def test__given_run_returns_valid_version__is_met__returns_true(self, valid_version: str) -> None:
        process_result = ProcessResultFactory.succeeded(valid_version)
        expect(self.process, times=1).run(self.RUN, capture_output=True).thenReturn(process_result)

        actual = self.subject.is_met(self.process)

        self.assertTrue(actual)

    @parameterized.expand(
        [  # type: ignore
            ("Python 2.1.0\n",),
            ("Docker version 0.10.11 build dea9396\n",),
            ("Docker version 0.10.11 with valid random version 5.4.1\n",),
            ("git version 1.35.1.windows2\n",),
            ("pip 1.2.3 from windows/path (python 3.9)\n",),
            ("curl 1.79.1 (x86_64-apple) libcurl/291.79.1 LibreSL/3.3.5",),
            ("GNU bash, version 1.2.57(1)-release (x86_64-apple)",),
            ("",),
            ("i dont even contain a version",),
            ("i contain a partial valid version 3.6 should still fail",),
        ]
    )
    def test__given_run_returns_invalid_version__is_met__returns_false(self, invalid_version: str) -> None:
        process_result = ProcessResultFactory.succeeded(stdout=invalid_version)
        expect(self.process, times=1).run(self.RUN, capture_output=True).thenReturn(process_result)

        actual = self.subject.is_met(self.process)

        self.assertFalse(actual)

    def test__given_run_fails__is_met__returns_false(self) -> None:
        process_result = ProcessResultFactory.failed()
        expect(self.process, times=1).run(ANY(str), capture_output=True).thenReturn(process_result)

        actual = self.subject.is_met(self.process)

        self.assertFalse(actual)

    @parameterized.expand(
        [  # type: ignore
            ("without help", {"name": "NAME", "run_cmd": "RUN", "version": "^1.0.0"}),
            (
                "with help",
                {
                    "name": "NAME",
                    "run_cmd": "RUN",
                    "version": ">1.0.0,<3.0.0",
                    "help": "HELP",
                },
            ),
        ]
    )
    def test__given_valid_args__parse__does_not_raise_error(self, _: str, data: Dict[str, str]) -> None:
        SemVerRequirement.parse_obj(data)

    def test__given_invalid_args__parse__raises_error(self) -> None:
        data = {
            "name": "",
            "run_cmd": "",
            "version": "invalid",
            "help": "",
            "extra": "field",
        }
        expected = [
            PydanticErrorUtils.empty("name"),
            PydanticErrorUtils.empty_cmd("run_cmd"),
            PydanticErrorUtils.invalid_sem_ver("version"),
            PydanticErrorUtils.empty("help"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            SemVerRequirement.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
