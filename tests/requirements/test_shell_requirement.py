import unittest
from typing import Dict

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions
from parameterized import parameterized
from pydantic import ValidationError

from bin.process.process import Process
from bin.requirements.shell_requirement import ShellRequirement
from tests.process.factories import ProcessResultFactory
from tests.pydantic_utils import PydanticErrorUtils


class ShellRequirementTests(unittest.TestCase):
    NAME = "Shell"
    MET_IF = "something runs | grep value"

    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

        self.subject = ShellRequirement(name=self.NAME, met_if_cmd=self.MET_IF)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_run_is_successful__is_met__returns_true(self) -> None:
        process_result = ProcessResultFactory.succeeded()
        expect(self.process, times=1).run(self.MET_IF, capture_output=True).thenReturn(process_result)

        actual = self.subject.is_met(self.process)

        self.assertTrue(actual)

    def test__given_run_fails__is_met__returns_false(self) -> None:
        process_result = ProcessResultFactory.failed()
        expect(self.process, times=1).run(self.MET_IF, capture_output=True).thenReturn(process_result)

        actual = self.subject.is_met(self.process)

        self.assertFalse(actual)

    @parameterized.expand(
        [  # type: ignore
            ("without help", {"name": "NAME", "met_if_cmd": "MET IF"}),
            ("with help", {"name": "NAME", "met_if_cmd": "MET_IF", "help": "HELP"}),
        ]
    )
    def test__given_valid_args__parse__does_not_raise_error(self, _: str, data: Dict[str, str]) -> None:
        ShellRequirement.parse_obj(data)

    def test__given_invalid_args__parse__raises_error(self) -> None:
        data = {"name": "", "met_if_cmd": "", "help": "", "extra": "field"}
        expected = [
            PydanticErrorUtils.empty("name"),
            PydanticErrorUtils.empty_cmd("met_if_cmd"),
            PydanticErrorUtils.extra("extra"),
            PydanticErrorUtils.empty("help"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            ShellRequirement.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
