import unittest
from pathlib import Path

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions

from bin.bin import Bin
from bin.bin_file.dtos.bin_file_mapper import BinFileMapper
from bin.bin_file.errors import BinFileParsingError
from bin.bin_file.init_command import InitCommand
from bin.commands.command import Command
from bin.commands.internal.factory import InternalCommandFactory
from bin.custom_commands.command_tree import CommandTree


class BinTests(unittest.TestCase):
    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_bin_file_contains_a_file__make_command__returns_bin_command(
        self,
    ) -> None:
        bin_command = mock(Command)
        path = Path("bin.yml")
        expect(BinFileMapper, times=1).contains_bin_file(path).thenReturn(True)
        expect(BinFileMapper, times=1).to_command(path).thenReturn(bin_command)

        actual = Bin.make_command(path)

        self.assertEqual(bin_command, actual)

    def test__given_bin_file_not_found__make_command__returns_cmd_tree_with_init_command(
        self,
    ) -> None:
        path = Path("not_found")
        expect(BinFileMapper, times=1).contains_bin_file(path).thenReturn(False)

        expected = CommandTree(
            self_cmd=InternalCommandFactory.help_only(
                "Bin helps you setup your project. Use the <init> command to create an example file.",
            ),
            subcommands_tree={"init": InitCommand.init(path)},
        )

        actual = Bin.make_command(path)

        self.assertEqual(expected, actual)

    def test__given_bin_file_parsing_error__make_command__raises_the_same_error(
        self,
    ) -> None:
        path = Path("bad_file")
        expected = BinFileParsingError("bad_file")
        expect(BinFileMapper, times=1).contains_bin_file(path).thenReturn(True)
        expect(BinFileMapper, times=1).to_command(path).thenRaise(expected)

        with self.assertRaises(BinFileParsingError) as ctx:
            Bin.make_command(path)

        actual = ctx.exception

        self.assertEqual(expected, actual)
