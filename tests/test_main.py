import unittest
from pathlib import Path
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.bin import Bin
from bin.commands.command import Command
from bin.commands.internal.factory import InternalCommandFactory
from bin.error_handler import ErrorHandler
from bin.main import Main
from bin.process.emoji import Emoji
from bin.process.process import Process


class MainTests(unittest.TestCase):
    def setUp(self) -> None:
        self.cwd = Path("current working directory")
        self.error_handler = mock(ErrorHandler, strict=True)
        self.command = mock(Command, strict=True)
        self.process = mock(Process, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            ("no args", [], f"  {Emoji.BIN}  bin \n"),
            ("help arg", ["--help"], f"  {Emoji.BIN}  bin --help\n"),
            ("one arg", ["one arg"], f'  {Emoji.BIN}  bin "one arg"\n'),
            (
                "help in multiple args",
                ["one", "--help", "two"],
                f"  {Emoji.BIN}  bin one --help two\n",
            ),
            (
                "multiple args",
                ["one", "two words", "three"],
                f'  {Emoji.BIN}  bin one "two words" three\n',
            ),
        ]
    )
    def test__given_command_is_successful__run__runs_main_command_with_them_and_returns_0(
        self, _: str, args: List[str], expected_command_log: str
    ) -> None:
        expected = 0
        expect(self.process, times=1).log_title(expected_command_log)
        expect(Bin, times=1).make_command(self.cwd).thenReturn(self.command)
        expect(self.command, times=1).run(self.process, args).thenReturn(self.process)
        subject = Main(self.error_handler, self.process, self.cwd, args)

        actual = subject.run()

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.error_handler)

    @parameterized.expand(
        [  # type: ignore
            ("no args", [], f"  {Emoji.BIN}  bin \n"),
            (
                "help in multiple args",
                ["one", "--help", "two"],
                f"  {Emoji.BIN}  bin one --help two\n",
            ),
            (
                "multiple args",
                ["one", "two words", "three"],
                f'  {Emoji.BIN}  bin one "two words" three\n',
            ),
        ]
    )
    def test__given_command_raises_error__run__calls_handler_and_exits_with_handler_return_code(
        self, _: str, args: List[str], expected_command_log: str
    ) -> None:
        expected = 20
        unmuted_process = mock(Process)
        expect(self.process, times=1).log_title(expected_command_log)
        expect(self.process, times=1).unmute_logs().thenReturn(unmuted_process)
        expect(Bin, times=1).make_command(self.cwd).thenReturn(self.command)
        error = ValueError("some error")
        expect(self.command, times=1).run(self.process, args).thenRaise(error)
        expect(self.error_handler, times=1).handle(error, self.command, unmuted_process).thenReturn(expected)
        subject = Main(self.error_handler, self.process, self.cwd, args)

        actual = subject.run()

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            ("no args", [], f"  {Emoji.BIN}  bin \n"),
            (
                "help in multiple args",
                ["one", "--help", "two"],
                f"  {Emoji.BIN}  bin one --help two\n",
            ),
            (
                "multiple args",
                ["one", "two words", "three"],
                f'  {Emoji.BIN}  bin one "two words" three\n',
            ),
        ]
    )
    def test__given_bin_command_creation_failed__run__calls_handler_and_exits_with_handler_return_code(
        self, _: str, args: List[str], expected_command_log: str
    ) -> None:
        expected = 30
        unmuted_process = mock(Process)
        expect(self.process, times=1).log_title(expected_command_log)
        expect(self.process, times=1).unmute_logs().thenReturn(unmuted_process)
        error = Exception("some error")
        expect(Bin, times=1).make_command(self.cwd).thenRaise(error)
        expect(self.error_handler, times=1).handle(error, InternalCommandFactory.noop(), unmuted_process).thenReturn(
            expected
        )
        subject = Main(self.error_handler, self.process, self.cwd, args)

        actual = subject.run()

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.command)
