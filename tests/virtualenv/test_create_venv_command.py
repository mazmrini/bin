import unittest
from typing import List

from mockito import expect, mock, unstub, verify, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.errors import CommandFailedError
from bin.process.emoji import Emoji
from bin.process.process import Process
from bin.virtualenv.create_venv_command import CreateVenvCommand
from tests.process.factories import ProcessResultFactory
from tests.test_cmd_utils import TestHelpCommand
from tests.virtualenv.factories import VenvFactory


class CreateVenvCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.venv = VenvFactory.venv()
        self.process = mock(Process, strict=True)

        self.subject = CreateVenvCommand.from_venv(self.venv)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_no_venv__run__creates_it(self) -> None:
        self._given_no_venv()
        expect(self.process, times=1).run("echo create", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded()
        )
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  Create venv")

        actual = self.subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    def test__given_no_venv_and_venv_creation_fails__run__raises_error(self) -> None:
        self._given_no_venv()
        expect(self.process, times=1).run("echo create", capture_output=True).thenReturn(ProcessResultFactory.failed())
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  Create venv")
        expected = CommandFailedError.command_failed("create_venv")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    def test__given_venv_exists__run__logs_info_but_does_not_run_create(self) -> None:
        expect(self.process, times=1).run("echo exists", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded()
        )
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  Create venv: already exists")

        actual = self.subject.run(self.process, [])

        self.assertEqual(self.process, actual)
        verify(self.process, times=0).run(self.venv.create_cmd)

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_command_with_args__run__raises_error(self, _: str, args: List[str]) -> None:
        expected = CommandFailedError.must_run_without_args("create_venv")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_help__run__logs_help(self) -> None:
        TestHelpCommand.assert_help_runs(self.subject, self.process)

    def _given_no_venv(self) -> None:
        expect(self.process, times=1).run("echo exists", capture_output=True).thenReturn(ProcessResultFactory.failed())
