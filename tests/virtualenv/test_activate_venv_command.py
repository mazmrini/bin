import unittest
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.errors import CommandFailedError
from bin.process.emoji import Emoji
from bin.process.process import Process
from bin.virtualenv.activate_venv_command import ActivateVenvCommand
from tests.process.factories import ProcessResultFactory
from tests.test_cmd_utils import TestHelpCommand
from tests.virtualenv.factories import VenvFactory


class ActivateVenvCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.venv = VenvFactory.venv()
        self.process = mock(Process, strict=True)

        self.subject = ActivateVenvCommand.from_venv(self.venv)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_activable_venv__run__returns_venv_activated_process(self) -> None:
        self._given_venv()
        venv_process = mock(Process)
        expect(self.process, times=1).run("echo activate", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded()
        )
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  Activate venv")
        expect(self.process, times=1).with_venv("echo activate").thenReturn(venv_process)

        actual = self.subject.run(self.process, [])

        self.assertEqual(venv_process, actual)

    def test__given_venv_but_activation_fails__run__raises_error(self) -> None:
        self._given_venv()
        expect(self.process, times=1).run("echo activate", capture_output=True).thenReturn(
            ProcessResultFactory.failed()
        )
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  Activate venv")
        expected = CommandFailedError.command_failed("activate_venv")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    def test__given_no_venv__run__raises_error(self) -> None:
        expect(self.process, times=1).run("echo exists", capture_output=True).thenReturn(ProcessResultFactory.failed())
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  Activate venv")
        expected = CommandFailedError.failed_with_reason("activate_venv", "venv not found")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_args__run__raises_error(self, _: str, args: List[str]) -> None:
        expected = CommandFailedError.must_run_without_args("activate_venv")

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_help__run__logs_help(self) -> None:
        TestHelpCommand.assert_help_runs(self.subject, self.process)

    def _given_venv(self) -> None:
        expect(self.process, times=1).run("echo exists", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded()
        )
