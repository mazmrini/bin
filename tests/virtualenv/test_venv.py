import unittest

from pydantic import ValidationError

from bin.virtualenv.venv import Venv
from tests.pydantic_utils import PydanticErrorUtils


class VenvTests(unittest.TestCase):
    def test__given_valid_args__parse__does_not_raise_error(self) -> None:
        Venv.parse_obj(
            {
                "create_cmd": "CREATE",
                "exists_cmd": "EXISTS",
                "activate_cmd": "ACTIVATE",
                "remove_cmd": "REMOVE",
            }
        )

    def test__given_invalid_args__init__raises_error(self) -> None:
        data = {
            "create_cmd": "",
            "exists_cmd": "",
            "activate_cmd": "",
            "remove_cmd": "",
            "extra": "field",
        }
        expected = [
            PydanticErrorUtils.empty_cmd("create_cmd"),
            PydanticErrorUtils.empty_cmd("exists_cmd"),
            PydanticErrorUtils.empty_cmd("activate_cmd"),
            PydanticErrorUtils.empty_cmd("remove_cmd"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            Venv.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
