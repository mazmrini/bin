from bin.virtualenv.venv import Venv


class VenvFactory:
    @staticmethod
    def venv() -> Venv:
        return Venv(
            create_cmd="echo create",
            activate_cmd="echo activate",
            exists_cmd="echo exists",
            remove_cmd="echo remove",
        )

    @staticmethod
    def yaml() -> str:
        return (
            "venv:\n"
            "  create: echo create\n"
            "  activate: echo activate\n"
            "  exists: echo exists\n"
            "  remove: echo remove\n"
        )
