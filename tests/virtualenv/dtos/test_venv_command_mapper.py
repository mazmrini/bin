import unittest

import yaml
from pydantic import BaseModel

from bin.custom_commands.command_tree import CommandTree
from bin.virtualenv.activate_venv_command import ActivateVenvCommand
from bin.virtualenv.create_venv_command import CreateVenvCommand
from bin.virtualenv.dtos import VenvDto
from bin.virtualenv.dtos.venv_command_mapper import VenvCommandMapper
from bin.virtualenv.remove_venv_command import RemoveVenvCommand
from tests.virtualenv.factories import VenvFactory


class TestModel(BaseModel):
    venv: VenvDto


class VenvCommandMapperTest(unittest.TestCase):
    def setUp(self) -> None:
        self.venv_yaml = VenvFactory.yaml()
        self.venv = VenvFactory.venv()

    def test__given_venv__to_create_venv_command__returns_create_venv_command(
        self,
    ) -> None:
        data = yaml.safe_load(self.venv_yaml)
        expected = CreateVenvCommand.from_venv(self.venv)

        actual = VenvCommandMapper.to_create_venv_command(TestModel.parse_obj(data).venv)

        self.assertEqual(expected, actual)

    def test__given_venv__to_activate_venv_command__returns_activate_venv_command(
        self,
    ) -> None:
        data = yaml.safe_load(self.venv_yaml)
        expected = ActivateVenvCommand.from_venv(self.venv)

        actual = VenvCommandMapper.to_activate_venv_command(TestModel.parse_obj(data).venv)

        self.assertEqual(expected, actual)

    def test__given_valid_venv_dto__to_command_dict__returns_command_tree(self) -> None:
        data = yaml.safe_load(self.venv_yaml)

        expected = {
            "super_venv": CommandTree(
                subcommands_tree={
                    "create": CreateVenvCommand.from_venv(self.venv),
                    "activate": ActivateVenvCommand.from_venv(self.venv),
                    "remove": RemoveVenvCommand.from_venv(self.venv),
                    "rm": RemoveVenvCommand.from_venv(self.venv),
                }
            )
        }

        actual = VenvCommandMapper.to_command_dict("super_venv", TestModel.parse_obj(data).venv)

        self.assertTrue(expected == actual)
