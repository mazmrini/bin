import unittest

import yaml
from pydantic import ValidationError

from bin.virtualenv.dtos.venv_dto import VenvDto
from tests.pydantic_utils import PydanticErrorUtils


class VenvDtoTests(unittest.TestCase):
    def test__given_invalid_venv_dto__parse__raise_errors(self) -> None:
        data = """
        create: ""
        activate: ""
        exists: ""
        remove: ""
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty_cmd("create"),
            PydanticErrorUtils.empty_cmd("activate"),
            PydanticErrorUtils.empty_cmd("exists"),
            PydanticErrorUtils.empty_cmd("remove"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            VenvDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
