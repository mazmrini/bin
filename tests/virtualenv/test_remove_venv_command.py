import unittest
from typing import List

from mockito import expect, mock, unstub, verify, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.errors import CommandFailedError
from bin.process.emoji import Emoji
from bin.process.process import Process
from bin.virtualenv.remove_venv_command import RemoveVenvCommand
from tests.process.factories import ProcessResultFactory
from tests.test_cmd_utils import TestHelpCommand
from tests.virtualenv.factories import VenvFactory


class RemoveVenvCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.venv = VenvFactory.venv()
        self.process = mock(Process, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_venv__run__removes_it(self) -> None:
        expect(self.process, times=1).run("echo exists", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded()
        )
        expect(self.process, times=1).run("echo remove", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded()
        )
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  Remove venv")
        subject = RemoveVenvCommand.from_venv(self.venv)

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    def test__given_venv_but_removes_failed__run__raises_error(self) -> None:
        expect(self.process, times=1).run("echo exists", capture_output=True).thenReturn(
            ProcessResultFactory.succeeded()
        )
        expect(self.process, times=1).run("echo remove", capture_output=True).thenReturn(ProcessResultFactory.failed())
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  Remove venv")
        subject = RemoveVenvCommand.from_venv(self.venv)
        expected = CommandFailedError.command_failed("remove_venv")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    def test__given_no_venv__run__logs_info_but_does_nothing(self) -> None:
        expect(self.process, times=1).run("echo exists", capture_output=True).thenReturn(ProcessResultFactory.failed())
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  Remove venv: no venv to remove")
        subject = RemoveVenvCommand.from_venv(self.venv)

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)
        verify(self.process, times=0).run("echo remove")

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_remove_command_with_args__run__raises_error(self, _: str, args: List[str]) -> None:
        subject = RemoveVenvCommand.from_venv(self.venv)
        expected = CommandFailedError.must_run_without_args("remove_venv")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_remove_command__run_help__logs_help(self) -> None:
        venv = VenvFactory.venv()
        subject = RemoveVenvCommand.from_venv(venv)

        TestHelpCommand.assert_help_runs(subject, self.process)
