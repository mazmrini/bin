from typing import Dict, List

from bin.commands.command import Command
from bin.commands.command_help import CommandHelp
from bin.custom_commands.command_tree import CommandTree
from bin.process.helpable import Helpable
from bin.process.process import Process


class DescCommandFake(Command):
    def __init__(self, desc: str) -> None:
        self._desc = desc

    def _run(self, process: Process, args: List[str]) -> Process:
        return process

    def _get_short_description(self) -> str:
        return self._desc

    def _get_help(self) -> Helpable:
        return CommandHelp.only_self(self._get_short_description())

    def expected_help(self) -> Helpable:
        return self._get_help()


class CommandTestFactory:
    @staticmethod
    def short_desc(text: str) -> DescCommandFake:
        return DescCommandFake(text)

    @staticmethod
    def useless_tree() -> Dict[str, Command]:
        return {
            "one": CommandTestFactory.short_desc("one"),
            "two": CommandTree(
                self_cmd=CommandTestFactory.short_desc("useless two"),
                subcommands_tree={
                    "one": CommandTestFactory.short_desc("two_one"),
                    "two": CommandTestFactory.short_desc("two_two"),
                    "three": CommandTree(subcommands_tree={"inner": CommandTestFactory.short_desc("two_three_inner")}),
                },
            ),
            "three": CommandTree(
                self_cmd=CommandTestFactory.short_desc("useless three"),
                subcommands_tree={
                    "one": CommandTestFactory.short_desc("alone"),
                    "two": CommandTestFactory.short_desc("three_two"),
                    "three": CommandTree(
                        subcommands_tree={"inner": CommandTestFactory.short_desc("three_three_inner")}
                    ),
                },
            ),
        }
