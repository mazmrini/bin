import unittest
from typing import List, Tuple

from parameterized import parameterized

from bin.commands.command_help import CommandHelp, CommandSummary


class CommandHelpTests(unittest.TestCase):
    @parameterized.expand(
        [  # type: ignore
            (
                "simple",
                100,
                [
                    ("should_be_last", "right_0"),
                    ("left_1", "right_1"),
                    ("left_2", "right_2"),
                ],
                [
                    "usage:",
                    "  self_help",
                    "",
                    "commands:",
                    "  left_1            right_1",
                    "  left_2            right_2",
                    "  should_be_last    right_0",
                ],
            ),
            (
                "takes biggest left as reference",
                100,
                [
                    ("b_longer_left_2", "right_2"),
                    ("a_left_1", "right_1"),
                    ("c_left_3", "right_3"),
                ],
                [
                    "usage:",
                    "  self_help",
                    "",
                    "commands:",
                    "  a_left_1           right_1",
                    "  b_longer_left_2    right_2",
                    "  c_left_3           right_3",
                ],
            ),
            (
                "simple multiline splits on words",
                25,
                [
                    ("c_left_3", "first line second line"),
                    ("a_left_1", "first line second line"),
                    ("b_left_2", "first line second line third line single"),
                ],
                [
                    "usage:",
                    "  self_help",
                    "",
                    "commands:",
                    "  a_left_1    first line",
                    "              second line",
                    "  b_left_2    first line",
                    "              second line",
                    "              third line",
                    "              single",
                    "  c_left_3    first line",
                    "              second line",
                ],
            ),
            (
                "complex multiline splits on words",
                35,
                [
                    ("a_left_1", "first line second line"),
                    ("b_longest left 2", "first line second line third line"),
                    ("c_left_3", "first line second line single"),
                ],
                [
                    "usage:",
                    "  self_help",
                    "",
                    "commands:",
                    "  a_left_1            first line",
                    "                      second line",
                    "  b_longest left 2    first line",
                    "                      second line",
                    "                      third line",
                    "  c_left_3            first line",
                    "                      second line",
                    "                      single",
                ],
            ),
            (
                "no pretty print is possible because of longest left+right word, print naively with 2 spaces",
                25,
                [
                    ("aaaa_1", "first line and some more blurs should not be cute"),
                    (
                        "b_2",
                        "because of this very_very_long_word of 19 chars plus 14 of the third left",
                    ),
                    ("longest_left_3", "some small string"),
                ],
                [
                    "usage:",
                    "  self_help",
                    "",
                    "commands:",
                    "  aaaa_1  first line and some more blurs should not be cute",
                    "  b_2  because of this very_very_long_word of 19 chars plus 14 of the third left",
                    "  longest_left_3  some small string",
                ],
            ),
            (
                "no pretty print is possible because of left, prints naively with 2 spaces",
                25,
                [
                    ("aaaa_1", "a b c"),
                    ("longest_longest_left_2", "a b c"),
                    ("z_3", "a b c"),
                ],
                [
                    "usage:",
                    "  self_help",
                    "",
                    "commands:",
                    "  aaaa_1  a b c",
                    "  longest_longest_left_2  a b c",
                    "  z_3  a b c",
                ],
            ),
        ]
    )
    def test__given_terminal_size_and_summaries__format_help__is_formatted_properly(
        self,
        _: str,
        max_cols: int,
        left_right: List[Tuple[str, str]],
        expected_lines: List[str],
    ) -> None:
        expected = "\n".join(expected_lines)
        summaries = [self._to_command_summary(l_r) for l_r in left_right]
        subject = CommandHelp(help="self_help", summaries=summaries)

        actual = subject.format_help(max_cols)

        self.assertEqual(expected, actual)

    @parameterized.expand([("long max_cols", 1000), ("short max cols", 1)])  # type: ignore
    def test__given_no_summaries_and_any_nb_cols__format_help__returns_naive_formatting(
        self, _: str, max_cols: int
    ) -> None:
        expected = "usage:\n  only self text"
        subject = CommandHelp.only_self("only self text")

        actual = subject.format_help(max_cols)

        self.assertEqual(expected, actual)

    def _to_command_summary(self, left_right: Tuple[str, str]) -> CommandSummary:
        left, right = left_right
        return CommandSummary(command=left, help=right)
