import unittest
from typing import List

from mockito import mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.command_help import CommandHelp
from bin.commands.errors import CommandNotFound
from bin.commands.internal.help_only_command import HelpOnlyCommand
from bin.process.process import Process
from tests.test_cmd_utils import TestHelpCommand


class HelpOnlyCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.process = mock(Process, strict=True)
        self.helpable = CommandHelp.only_self("Only help")
        self.subject = HelpOnlyCommand(short_description="Short description", help=self.helpable)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_help__run__logs_help_through_the_template_pattern(self) -> None:
        TestHelpCommand.assert_help_runs(self.subject, self.process, self.helpable)

    @parameterized.expand(
        [  # type: ignore
            ("no args", []),
            ("one arg", ["one"]),
            ("multiple args", ["one", "two", "three"]),
            ("help not first", ["one", "--help", "three"]),
        ]
    )
    def test__given_args_that_are_not_help__run__raises_command_not_found(self, _: str, args: List[str]) -> None:
        with self.assertRaises(CommandNotFound):
            self.subject.run(self.process, args)

        verifyZeroInteractions(self.process)
