import unittest

from bin.commands.internal.apply_env_command import ApplyEnvCommand
from bin.commands.internal.factory import InternalCommandFactory
from bin.commands.internal.with_setup_command import WithSetupCommand


class InternalCommandFactoryTests(unittest.TestCase):
    def test__given_env__with_env__returns_wrapped_command(self) -> None:
        env = {"ONE": "var_1", "TWO": "var_2", "THREE": "var_3"}
        main_cmd = InternalCommandFactory.unmute_logs()
        expected = WithSetupCommand(setup_cmds=[ApplyEnvCommand(env=env)], main_cmd=main_cmd)

        actual = InternalCommandFactory.wrap_with_env(env, main_cmd)

        self.assertEqual(expected, actual)

    def test__given_empty_env__with_env__returns_command_as_is(self) -> None:
        main_cmd = InternalCommandFactory.unmute_logs()
        expected = main_cmd

        actual = InternalCommandFactory.wrap_with_env({}, main_cmd)

        self.assertEqual(expected, actual)
