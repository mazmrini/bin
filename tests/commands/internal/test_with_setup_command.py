import unittest
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.command import Command
from bin.commands.internal.with_setup_command import WithSetupCommand
from bin.process.process import Process
from tests.commands.factories import CommandTestFactory
from tests.test_cmd_utils import TestHelpCommand


class WithSetupCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.main_cmd = mock(Command, strict=True)
        self.process = mock(Process, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [
            ("no args", []),
            ("one arg", ["one"]),
            ("multiple args", ["one", "two", "three"]),
        ]  # type: ignore
    )
    def test__given_successful_commands__run_only_passes_args_to_main__returns_main_command_process(
        self, _: str, args: List[str]
    ) -> None:
        first_cmd = mock(Command, strict=True)
        first_process = mock(Process, strict=True)
        expect(first_cmd, times=1).run(self.process, []).thenReturn(first_process)
        second_cmd = mock(Command, strict=True)
        second_process = mock(Process, strict=True)
        expect(second_cmd, times=1).run(first_process, []).thenReturn(second_process)
        main_process = mock(Process, strict=True)
        expect(self.main_cmd, times=1).run(second_process, args).thenReturn(main_process)
        subject = WithSetupCommand(setup_cmds=[first_cmd, second_cmd], main_cmd=self.main_cmd)

        actual = subject.run(self.process, args)

        self.assertEqual(main_process, actual)

    @parameterized.expand(
        [
            ("no args", []),
            ("one arg", ["one"]),
            ("multiple args", ["one", "two", "three"]),
        ]  # type: ignore
    )
    def test__given_command_raises_error__run__stops_executing_and_raises_the_same_error(
        self, _: str, args: List[str]
    ) -> None:
        first_cmd = mock(Command, strict=True)
        first_process = mock(Process, strict=True)
        expect(first_cmd, times=1).run(self.process, []).thenReturn(first_process)
        failed_cmd = mock(Command, strict=True)
        expect(failed_cmd, times=1).run(first_process, []).thenRaise(ValueError)
        subject = WithSetupCommand(
            setup_cmds=[first_cmd, failed_cmd, mock(Command), mock(Command)],
            main_cmd=self.main_cmd,
        )

        with self.assertRaises(ValueError):
            subject.run(self.process, args)

        verifyZeroInteractions(self.main_cmd)

    def test__given_help__run__logs_main_cmd_help(self) -> None:
        cmd = CommandTestFactory.short_desc("main_help")

        subject = WithSetupCommand(setup_cmds=[mock(Command), mock(Command)], main_cmd=cmd)

        TestHelpCommand.assert_help_runs(subject, self.process, cmd.expected_help())
