import unittest
from typing import List

from mockito import ANY, expect, mock, unstub, verify, verifyNoUnwantedInteractions
from parameterized import parameterized

from bin.commands.command import Command
from bin.commands.command_help import CommandHelp
from bin.process.helpable import Helpable
from bin.process.process import Process
from tests.process.factories import ProcessResultFactory


class TestCommand(Command):
    def __init__(self, command_process: Process) -> None:
        self.command_process = command_process

    def _run(self, process: Process, args: List[str]) -> Process:
        process.run("test_cmd", args)

        return self.command_process

    def _get_short_description(self) -> str:
        return "Test command short description"

    def _get_help(self) -> Helpable:
        return CommandHelp.only_self("Test command help")

    def expected_help(self) -> Helpable:
        return self._get_help()


class CommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.process = mock(Process, strict=True)
        self.command_process = mock(Process)
        self.subject = TestCommand(self.command_process)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            ("no args", []),
            ("one arg", ["one"]),
            ("multiple args", ["one", "two", "three"]),
            ("help not first", ["one", "--help", "other"]),
        ]
    )
    def test__given_no_help_args__run__calls_cmd_and_returns_command_process(self, _: str, args: List[str]) -> None:
        expect(self.process, times=1).run("test_cmd", args).thenReturn(ProcessResultFactory.succeeded())

        actual = self.subject.run(self.process, args)

        self.assertEqual(self.command_process, actual)
        verify(self.process, times=0).log_help(ANY)

    @parameterized.expand(
        [  # type: ignore
            ("-h", ["-h"]),
            ("-h one arg", ["-h", "one"]),
            ("-h multiple args", ["-h", "one", "two", "three"]),
            ("--help", ["--help"]),
            ("--help one arg", ["--help", "one"]),
            ("--help multiple args", ["--help", "one", "two", "three"]),
        ]
    )
    def test__given_help_is_first_arg__run___logs_cmd_help_and_returns_process(self, _: str, args: List[str]) -> None:
        expected_help = self.subject.expected_help()
        expect(self.process, times=1).log_help(expected_help)

        actual = self.subject.run(self.process, args)

        self.assertEqual(self.process, actual)
        verify(self.process, times=0).run(ANY, ANY)
