import unittest

import yaml
from parameterized import parameterized
from pydantic import BaseModel, ValidationError

from bin.models.sem_ver_spec import SemVerSpec


class TestModel(BaseModel):
    spec: SemVerSpec


class SemVerSpecTests(unittest.TestCase):
    @parameterized.expand(
        [  # type: ignore
            ("0.0.0",),
            ("==1.0.0",),
            (">1.0.0",),
            (">=1.0.0",),
            ("<1.3.33",),
            ("<=1.3.33",),
            (">1.0.0,<=2.0.0",),
            (">=1.0.0,<3.1.0",),
            (">=1.0.0,<3.1.0",),
            ("~1.3",),
            ("^1.3",),
            ("<2.0.0,>3.0.0",),  # invalid spec is fine by the lib
        ]
    )
    def test__valid_sem_ver_spec__validation__is_noop(self, value: str) -> None:
        TestModel(spec=value)

    @parameterized.expand(
        [  # type: ignore
            ("text", "spec: invalid"),
            ("none", "spec:"),
            ("empty str", 'spec: ""'),
            (
                "invalid version",
                'spec: "+1.0.0"',
            ),
            (
                "list",
                """
        spec:
          - something
          - other
        """,
            ),
            (
                "dict",
                """
        spec:
          attribute: nice
          other: cool
        """,
            ),
        ]
    )
    def test__invalid_sem_ver_spec__validation__raises_error(self, _: str, data: str) -> None:
        data = yaml.safe_load(data)
        with self.assertRaises(ValidationError):
            TestModel.parse_obj(data)

    @parameterized.expand(
        [  # type: ignore
            (
                "3.1.0",
                "3.1.0",
            ),
            (
                "3.1.0",
                "==3.1.0",
            ),
            (
                "3.1.0",
                ">2.1.0,<5.0.0",
            ),
        ]
    )
    def test__version_in_spec__contains__returns_true(self, version: str, spec: str) -> None:
        spec = SemVerSpec(spec)

        actual = version in spec

        self.assertTrue(actual)

    @parameterized.expand(
        [  # type: ignore
            (
                "1.1.0",
                "3.1.0",
            ),
            (
                "1.1.0",
                "==3.1.0",
            ),
            (
                "1.0.0",
                ">2.1.0,<5.0.0",
            ),
            ("im not even a version", ">1.0.0"),
            ("1.0.0", "not a spec"),
            ("1.0", "==1.0.0"),
        ]
    )
    def test__version_in_spec__contains__returns_false(self, version: str, spec: str) -> None:
        spec = SemVerSpec(spec)

        actual = version in spec

        self.assertFalse(actual)
