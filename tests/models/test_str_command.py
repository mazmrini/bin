import unittest

import yaml
from parameterized import parameterized
from pydantic import BaseModel, ValidationError

from bin.models.str_command import StrCommand


class TestModel(BaseModel):
    cmd: StrCommand


class StrCommandTests(unittest.TestCase):
    @parameterized.expand(
        [  # type: ignore
            ("str", "cmd: im a command", "im a command"),
            ("true string", 'cmd: "true"', "true"),
            ("literally true value", "cmd: true", "true"),
            ("literally false value", "cmd: false", "false"),
            ("int", "cmd: 33", "33"),
            ("float", "cmd: 10.15", "10.15"),
            (
                "pipe",
                "cmd: some valid command | with another one",
                "some valid command | with another one",
            ),
            ("semi column", "cmd: i end with;", "i end with;"),
            (
                "with spaces",
                'cmd: echo "multiple spaces here" | grep "something"',
                'echo "multiple spaces here" | grep "something"',
            ),
            (
                "starts with number",
                "cmd: 32base my file >> other.txt",
                "32base my file >> other.txt",
            ),
            (
                "contains weird symbols",
                "cmd: 32base m*A()A!@#*( >> other{*}&/*.txt",
                "32base m*A()A!@#*( >> other{*}&/*.txt",
            ),
            (
                "starts with bang then alpha",
                'cmd: "! test something"',
                "! test something",
            ),
            ("starts with bang then numerical", 'cmd: "! 32base"', "! 32base"),
            (
                "starts with root path",
                "cmd: /app/run/script with param",
                "/app/run/script with param",
            ),
            ("local script", "cmd: bin/run", "bin/run"),
            ("local script starting with .", "cmd: ./bin/run", "./bin/run"),
            ("relative script", "cmd: ../../bin/run", "../../bin/run"),
            (
                "windows path",
                r'cmd: "C:\\\\Desktop\\scripts\\cmd param"',
                r"C:\\Desktop\scripts\cmd param",
            ),
            (
                "windows relative path",
                r'cmd: "..\\..\\scripts\\run"',
                r"..\..\scripts\run",
            ),
        ]
    )
    def test__given_valid_str_cmd__parse__returns_expected(self, _: str, data: str, expected: str) -> None:
        subject = TestModel.parse_obj(yaml.safe_load(data))

        actual = subject.cmd

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            ("missing space", "cmd:sad", "TestModel expected dict not str"),
            ("nothing", "cmd:", "none is not an allowed value"),
            ("empty str", 'cmd: ""', "ensure command has at least 1 characters"),
            (
                "list",
                """
        cmd:
        - first item
        - second item
        """,
                "must be a string",
            ),
            (
                "dict",
                """
        cmd:
          attribute: nice
          other_attribute: cool
        """,
                "must be a string",
            ),
        ]
    )
    def tests__given_invalid_str_cmd__validation_raises_error(self, _: str, data: str, expected: str) -> None:
        data = yaml.safe_load(data)

        with self.assertRaises(ValidationError) as ctx:
            TestModel.parse_obj(data)

        actual = ctx.exception.errors()[0]["msg"]

        self.assertEqual(expected, actual)
