import unittest

from bin.models.bin_base_model import BinBaseModel


class ModelA(BinBaseModel):
    text: str
    number: int


class ModelB(BinBaseModel):
    number: int
    text: str


class InnerModel(BinBaseModel):
    a: BinBaseModel
    b: BinBaseModel


class BinBaseModelTests(unittest.TestCase):
    def test__given_same_class_with_same_values__eq__returns_true(self) -> None:
        one = ModelA(text="hello", number=20)
        other = ModelA(text="hello", number=20)

        self.assertTrue(one == one)
        self.assertTrue(other == other)
        self.assertTrue(one == other)
        self.assertTrue(other == one)

    def test__given_same_class_with_different_values__eq__returns_false(self) -> None:
        one = ModelA(text="bye", number=20)
        other = ModelA(text="hello", number=20)

        self.assertFalse(one == other)
        self.assertFalse(other == one)

    def test__given_different_classes_with_same_values__eq__returns_false(self) -> None:
        a = ModelA(text="hello", number=20)
        b = ModelB(text="hello", number=20)

        self.assertFalse(a == b)
        self.assertFalse(b == a)

    def test__given_inner_models_with_same_class_and_values__eq__returns_true(
        self,
    ) -> None:
        first = InnerModel(a=ModelA(text="hello", number=10), b=ModelB(text="hello", number=10))
        second = InnerModel(a=ModelA(text="hello", number=10), b=ModelB(text="hello", number=10))

        self.assertTrue(first == second)
        self.assertTrue(second == first)

    def test__given_inner_models_with_different_classes_but_same_values__eq__returns_false(
        self,
    ) -> None:
        first = InnerModel(a=ModelA(text="hello", number=10), b=ModelA(text="hello", number=10))
        second = InnerModel(a=ModelB(text="hello", number=10), b=ModelB(text="hello", number=10))

        self.assertFalse(first == second)
        self.assertFalse(second == first)
