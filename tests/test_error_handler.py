import unittest

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized
from pydantic import BaseModel, ValidationError

from bin.commands.command import Command
from bin.commands.errors import CommandFailedError, CommandNotFound, CommandParseError
from bin.env.errors import EnvProvisionError
from bin.error_handler import ErrorHandler, ExitCode, OnErrorCallback, get_error_handler
from bin.process.process import Process


class ErrorHandlerTests(unittest.TestCase):
    def setUp(self) -> None:
        self.command = mock(Command)
        self.process = mock(Process)
        self.subject = ErrorHandler()

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_error_is_registered__handle__calls_callback(self) -> None:
        expected = 22
        self.subject.register(ValueError, self._make_callback(11))
        self.subject.register(FileExistsError, self._make_callback(expected))
        self.subject.register(TypeError, self._make_callback(33))

        actual = self.subject.handle(FileExistsError(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__given_error_is_registered_multiple_times__handle__calls_latest_callback(
        self,
    ) -> None:
        expected = 10000
        self.subject.register(ValueError, self._make_callback(11))
        self.subject.register(FileExistsError, self._make_callback(22))
        self.subject.register(TypeError, self._make_callback(33))
        self.subject.register(FileExistsError, self._make_callback(44))
        self.subject.register(FileExistsError, self._make_callback(expected))

        actual = self.subject.handle(FileExistsError(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__given_exception_and_error_is_register__handle__calls_specific_error_callback(
        self,
    ) -> None:
        expected = 22
        self.subject.register(Exception, self._make_callback(11))
        self.subject.register(FileExistsError, self._make_callback(expected))

        actual = self.subject.handle(FileExistsError(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__given_only_exception_is_registered__handle__calls_exception_callback(
        self,
    ) -> None:
        expected = 22
        self.subject.register(Exception, self._make_callback(expected))

        actual = self.subject.handle(Exception(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__given_only_exception_is_registered__handle_with_different_error__calls_exception_callback(
        self,
    ) -> None:
        expected = 22
        self.subject.register(Exception, self._make_callback(expected))

        actual = self.subject.handle(ValueError(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__given_error_not_registered__handle__returns_1_exit_code(self) -> None:
        expected = 1
        self.subject.register(ValueError, self._make_callback(11))
        self.subject.register(TypeError, self._make_callback(33))

        actual = self.subject.handle(Exception(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__given_empty_handler__handle__returns_1_exit_code(self) -> None:
        expected = 1

        actual = self.subject.handle(Exception(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__default_handler_with_command_not_found__handle__calls_help_logs_error_and_returns_1(
        self,
    ) -> None:
        expected = 1
        exc = CommandNotFound()
        expect(self.command, times=1).run(self.process, ["--help"]).thenReturn(self.process)
        expect(self.process, times=1).log_error("\nerror: command not found (exit code 1)")
        subject = get_error_handler()

        actual = subject.handle(exc, self.command, self.process)

        self.assertEqual(expected, actual)

    def test__default_handler_with_command_not_found__handle_help_failed__logs_it_and_returns_1(
        self,
    ) -> None:
        expected = 1
        help_exc = ValueError("something failed :(")
        expect(self.command, times=1).run(self.process, ["--help"]).thenRaise(help_exc)
        expect(self.process, times=1).log_error(
            "error: command not found\n\nunexpected error happened when running --help (exit code 1)"
        )
        subject = get_error_handler()

        actual = subject.handle(CommandNotFound(), self.command, self.process)

        self.assertEqual(expected, actual)

    def test__default_handler_with_command_failed_error__handle__logs_it_and_returns_1(
        self,
    ) -> None:
        expected = 1
        exc = CommandFailedError("command failed :(")
        expect(self.process, times=1).log("command failed :(")
        expect(self.process, times=1).log_error("\nerror: command failed (exit code 1)")
        subject = get_error_handler()

        actual = subject.handle(exc, self.command, self.process)

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.command)

    def test__default_handler_with_command_parse_error__handle__logs_it_and_returns_1(
        self,
    ) -> None:
        expected = 1
        expected_msg = "fancy errors:\n  with a long message"
        exc = CommandParseError(expected_msg)
        expect(self.process, times=1).log(expected_msg)
        expect(self.process, times=1).log_error("\nerror: invalid bin file (exit code 1)")
        subject = get_error_handler()

        actual = subject.handle(exc, self.command, self.process)

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.command)

    def test__default_handler_with_validation_error__handle__logs_it_and_returns_1(
        self,
    ) -> None:
        expected = 1
        exc = ValidationError([], BaseModel)
        expect(self.process, times=1).log_error("error: invalid bin file (exit code 1)")
        subject = get_error_handler()

        actual = subject.handle(exc, self.command, self.process)

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.command)

    def test__default_handler_with_env_provision_error__handle__logs_it_and_returns_1(
        self,
    ) -> None:
        expected = 1
        exc = EnvProvisionError("ejson failed to decrypt")
        expect(self.process, times=1).log("ejson failed to decrypt")
        expect(self.process, times=1).log_error("\nerror: env loading failed (exit code 1)")
        subject = get_error_handler()

        actual = subject.handle(exc, self.command, self.process)

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.command)

    @parameterized.expand(
        [  # type: ignore
            ("exception", Exception("im an exception"), "im an exception"),
            ("key error", ValueError("key error"), "key error"),
        ]
    )
    def test__default_handler__handle_unbound_exceptions__logs_default_exception_message_and_returns_1(
        self, _: str, exc: Exception, expected_msg: str
    ) -> None:
        expected = 1
        expect(self.process, times=1).log(expected_msg)
        expect(self.process, times=1).log_error("\nerror: unhandled error (exit code 1)")
        subject = get_error_handler()

        actual = subject.handle(exc, self.command, self.process)

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.command)

    def _make_callback(self, expected: ExitCode) -> OnErrorCallback[Exception]:
        def callback(_: Exception, __: Command, ___: Process) -> ExitCode:
            return expected

        return callback
