from typing import Any, Dict, List, Union

from pydantic import ValidationError

Loc = Union[str, int]


class PydanticErrorUtils:
    @staticmethod
    def empty(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "ensure this value has at least 1 characters"}

    @staticmethod
    def required(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "field required"}

    @staticmethod
    def extra(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "extra fields not permitted"}

    @staticmethod
    def invalid_sem_ver(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "invalid semantic versioning spec"}

    @staticmethod
    def empty_cmd(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "ensure command has at least 1 characters"}

    @staticmethod
    def not_a_list(*loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": "value is not a valid list"}

    @staticmethod
    def custom(msg: str, *loc: Loc) -> Dict[str, Any]:
        return {"loc": loc, "msg": msg}

    @staticmethod
    def from_exception(exc: ValidationError) -> List[Dict[str, Any]]:
        return [{"loc": e["loc"], "msg": e["msg"]} for e in exc.errors()]
