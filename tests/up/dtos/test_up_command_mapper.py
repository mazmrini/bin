import unittest
from typing import List

import yaml
from pydantic import BaseModel

from bin.commands.command import Command
from bin.up.conditional_up_subcommand import ConditionalUpSubcommand
from bin.up.down_command import DownCommand
from bin.up.dtos import UpSubcommandDto
from bin.up.dtos.up_command_mapper import UpCommandMapper
from bin.up.simple_up_subcommand import SimpleUpSubcommand
from bin.up.up_command import UpCommand


class TestModel(BaseModel):
    up: List[UpSubcommandDto]


class UpCommandMapperTests(unittest.TestCase):
    def test__given_dtos_as_str__to_up_command__returns_expected_command(self) -> None:
        data = yaml.safe_load(self._yaml())

        actual = UpCommandMapper.to_up_command(TestModel.parse_obj(data).up)

        self.assertEqual(self._expected_up(), actual)

    def test__given_dtos_as_str__to_down_command__returns_expected_object(self) -> None:
        data = yaml.safe_load(self._yaml())

        actual = UpCommandMapper.to_down_command(TestModel.parse_obj(data).up)

        self.assertEqual(self._expected_down(), actual)

    def _yaml(self) -> str:
        return """
            up:
            # without name attribute
            - cmd 0
            - up: up 1
            - down: down 2
            - up: up 3
              down: down 3
            - up: up 4
              down: down 4
              down_unless: down unless 4
            - up: up 5
              up_unless: up unless 5
            - up: up 6
              up_unless: up unless 6
              down: down 6
            - up: up 7
              up_unless: up unless 7
              down: down 7
              down_unless: down unless 7
            - down: down 8
              down_unless: down unless 8
            # up + down
            - name: name 9
              up: up 9
            - name: name 10
              down: down 10
            - name: name 11
              up: up 11
              down: down 11
            # up + up_unless
            - name: name 12
              up: up 12
              up_unless: up unless 12
            - name: name 13
              up: up 13
              up_unless: up unless 13
              down: down 13
            - name: name 14
              up: up 14
              up_unless: up unless 14
              down: down 14
              down_unless: down unless 14
            # down + down_unless
            - name: name 15
              down: down 15
              down_unless: down unless 15
            - name: name 16
              up: up 16
              down: down 16
              down_unless: down unless 16
            """

    def _expected_up(self) -> Command:
        return UpCommand(
            subcommands=[
                SimpleUpSubcommand(name="cmd 0", run_cmd="cmd 0"),
                SimpleUpSubcommand(name="up 1", run_cmd="up 1"),
                SimpleUpSubcommand(name="up 3", run_cmd="up 3"),
                SimpleUpSubcommand(name="up 4", run_cmd="up 4"),
                ConditionalUpSubcommand(name="up 5", run_cmd="up 5", unless_cmd="up unless 5"),
                ConditionalUpSubcommand(name="up 6", run_cmd="up 6", unless_cmd="up unless 6"),
                ConditionalUpSubcommand(name="up 7", run_cmd="up 7", unless_cmd="up unless 7"),
                SimpleUpSubcommand(name="name 9", run_cmd="up 9"),
                SimpleUpSubcommand(name="name 11", run_cmd="up 11"),
                ConditionalUpSubcommand(name="name 12", run_cmd="up 12", unless_cmd="up unless 12"),
                ConditionalUpSubcommand(name="name 13", run_cmd="up 13", unless_cmd="up unless 13"),
                ConditionalUpSubcommand(name="name 14", run_cmd="up 14", unless_cmd="up unless 14"),
                SimpleUpSubcommand(name="name 16", run_cmd="up 16"),
            ]
        )

    def _expected_down(self) -> Command:
        return DownCommand(
            subcommands=[
                SimpleUpSubcommand(name="down 2", run_cmd="down 2"),
                SimpleUpSubcommand(name="down 3", run_cmd="down 3"),
                ConditionalUpSubcommand(name="down 4", run_cmd="down 4", unless_cmd="down unless 4"),
                SimpleUpSubcommand(name="down 6", run_cmd="down 6"),
                ConditionalUpSubcommand(name="down 7", run_cmd="down 7", unless_cmd="down unless 7"),
                ConditionalUpSubcommand(name="down 8", run_cmd="down 8", unless_cmd="down unless 8"),
                SimpleUpSubcommand(name="name 10", run_cmd="down 10"),
                SimpleUpSubcommand(name="name 11", run_cmd="down 11"),
                SimpleUpSubcommand(name="name 13", run_cmd="down 13"),
                ConditionalUpSubcommand(name="name 14", run_cmd="down 14", unless_cmd="down unless 14"),
                ConditionalUpSubcommand(name="name 15", run_cmd="down 15", unless_cmd="down unless 15"),
                ConditionalUpSubcommand(name="name 16", run_cmd="down 16", unless_cmd="down unless 16"),
            ]
        )
