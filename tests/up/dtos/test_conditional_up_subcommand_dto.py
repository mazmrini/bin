import unittest

import yaml
from parameterized import parameterized
from pydantic import ValidationError

from bin.up.dtos.conditional_up_subcommand_dto import ConditionalUpSubcommandDto
from tests.pydantic_utils import PydanticErrorUtils


class ConditionalUpSubcommandDtoTests(unittest.TestCase):
    def test__given_invalid_conditional_up_subcommand_dto__parse__raises_error(
        self,
    ) -> None:
        data = """
        name: ""
        up: ""
        down: ""
        up_unless: ""
        down_unless: ""
        extra: field
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty("name"),
            PydanticErrorUtils.empty_cmd("up"),
            PydanticErrorUtils.empty_cmd("down"),
            PydanticErrorUtils.empty_cmd("up_unless"),
            PydanticErrorUtils.empty_cmd("down_unless"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            ConditionalUpSubcommandDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            (
                "only name",
                """
        name: yo
        """,
                "up or down is required",
            ),
            (
                "only unlesses",
                """
        name: yo
        up_unless: something
        down_unless: something
        """,
                "up or down is required",
            ),
            (
                "only up unless",
                """
        name: yo
        up_unless: something
        """,
                "up or down is required",
            ),
            (
                "up unless with only down",
                """
        up_unless: something
        down: valid
        """,
                "up is required with up_unless",
            ),
            (
                "up unless with valid down + down unless",
                """
        name: yo
        up_unless: something
        down: im good
        down_unless: me too
        """,
                "up is required with up_unless",
            ),
            (
                "only down unless",
                """
        down_unless: something
        """,
                "up or down is required",
            ),
            (
                "down unless with only up",
                """
        name: yo
        up: valid
        down_unless: something
        """,
                "down is required with down_unless",
            ),
            (
                "down unless with valid up + up unless",
                """
        up: im good
        up_unless: me too
        down_unless: something
        """,
                "down is required with down_unless",
            ),
        ]
    )
    def test__invalid_dto__parse__raises_error(self, _: str, dto: str, expected_msg: str) -> None:
        data = yaml.safe_load(dto)
        expected = [PydanticErrorUtils.custom(expected_msg, "__root__")]

        with self.assertRaises(ValidationError) as ctx:
            ConditionalUpSubcommandDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertEqual(expected, actual)
