import unittest

from mockito import ANY, expect, mock, unstub, verify, verifyNoUnwantedInteractions
from pydantic import ValidationError

from bin.process.process import Process
from bin.up.conditional_up_subcommand import ConditionalUpSubcommand
from bin.up.up_subcommand import UpResult
from tests.process.factories import ProcessResultFactory
from tests.pydantic_utils import PydanticErrorUtils


class ConditionalUpSubcommandTest(unittest.TestCase):
    NAME = "cmd name"
    RUN = "RUN_COMMAND"
    UNLESS_CMD = "UNLESS_COMMAND"

    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

        self.subject = ConditionalUpSubcommand(name=self.NAME, run_cmd=self.RUN, unless_cmd=self.UNLESS_CMD)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_command_ran_and_fixes_unless__run__returns_success_result(
        self,
    ) -> None:
        expected = UpResult.succeeded(self.NAME)
        expect(self.process, times=2).run(self.UNLESS_CMD, capture_output=True).thenReturn(
            ProcessResultFactory.failed("need to run command")
        ).thenReturn(ProcessResultFactory.succeeded("succeeded on the second run"))
        expect(self.process, times=1).run(self.RUN).thenReturn(ProcessResultFactory.succeeded())

        actual = self.subject.run(self.process)

        self.assertEqual(expected, actual)

    def test__given_command_ran_but_did_not_fix_unless__run__returns_failed_result(
        self,
    ) -> None:
        expected = UpResult.failed(self.NAME).with_details(
            "Command 'RUN_COMMAND' ran but failed to fix 'UNLESS_COMMAND'"
        )
        expect(self.process, times=2).run(self.UNLESS_CMD, capture_output=True).thenReturn(
            ProcessResultFactory.failed("need to run command")
        ).thenReturn(ProcessResultFactory.failed("command did not fix me"))
        expect(self.process, times=1).run(self.RUN).thenReturn(ProcessResultFactory.succeeded())

        actual = self.subject.run(self.process)

        self.assertEqual(expected, actual)

    def test__given_command_ran_but_failed__run__returns_failed_result(self) -> None:
        expected = UpResult.failed(self.NAME).with_details("Command 'RUN_COMMAND' failed")
        expect(self.process, times=1).run(self.UNLESS_CMD, capture_output=True).thenReturn(
            ProcessResultFactory.failed("need to run command")
        )
        expect(self.process, times=1).run(self.RUN).thenReturn(ProcessResultFactory.failed())

        actual = self.subject.run(self.process)

        self.assertEqual(expected, actual)

    def test__given_command_does_not_need_to_run__run__returns_skipped_result_and_never_calls_the_command(
        self,
    ) -> None:
        expected = UpResult.succeeded(self.NAME).with_details("already up")
        expect(self.process, times=1).run(self.UNLESS_CMD, capture_output=True).thenReturn(
            ProcessResultFactory.succeeded("no need to run the command")
        )

        actual = self.subject.run(self.process)

        self.assertEqual(expected, actual)
        verify(self.process, times=0).run(ANY)

    def test__given_empty_args__parse__raises_error(self) -> None:
        data = {"name": "", "run_cmd": "", "unless_cmd": "", "extra": "field"}
        expected = [
            PydanticErrorUtils.empty("name"),
            PydanticErrorUtils.empty_cmd("run_cmd"),
            PydanticErrorUtils.empty_cmd("unless_cmd"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            ConditionalUpSubcommand.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertEqual(expected, actual)
