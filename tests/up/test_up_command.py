import unittest
from typing import Any, List

from mockito import ANY, expect, mock, unstub, verify, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.command_help import CommandHelp
from bin.commands.errors import CommandFailedError
from bin.process.emoji import Emoji
from bin.process.process import Process
from bin.up.up_command import UpCommand
from bin.up.up_subcommand import UpResult, UpSubcommand
from tests.test_cmd_utils import TestHelpCommand


class UpCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_only_successful_subcommands__run__outputs_expected_messages(
        self,
    ) -> None:
        subcommands = [
            self._subcommand(1, True),
            self._subcommand_with_details(2, True),
            self._subcommand(3, True),
        ]
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  1/3  name_1")
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  2/3  (details_2) name_2")
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  3/3  name_3")
        expect(self.process, times=1).log_success("Up ran successfully")
        subject = UpCommand(subcommands=subcommands)

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    def test__given_no_subcommands__run__is_noop_but_warns_the_user(self) -> None:
        subject = UpCommand(subcommands=[])
        expect(self.process, times=1).log_warning(f"{Emoji.WARNING}  No steps are defined")

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)
        verify(self.process, times=0).run(ANY)

    def test__given_successful_and_failing_subcommands__run__raises_error_at_the_first_failure(
        self,
    ) -> None:
        subcommands = [
            self._subcommand(1, True),
            self._subcommand(2, True),
            self._subcommand_with_details(3, False),
            self._subcommand(4, True, times=0),
            self._subcommand(5, False, times=0),
        ]
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  1/5  name_1")
        expect(self.process, times=1).log(f"{Emoji.SUCCESS}  2/5  name_2")
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  3/5  name_3\n  details_3")
        expect(self.process, times=1).log_error("Up failed")
        subject = UpCommand(subcommands=subcommands)
        expected = CommandFailedError.command_failed("up")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    def test__given_only_failing_subcommands__run__raises_error_at_the_first_failure(
        self,
    ) -> None:
        subcommands = [
            self._subcommand(1, False),
            self._subcommand_with_details(2, False, times=0),
            self._subcommand_with_details(3, False, times=0),
        ]
        expect(self.process, times=1).log(f"{Emoji.FAILURE}  1/3  name_1")
        expect(self.process, times=1).log_error("Up failed")
        subject = UpCommand(subcommands=subcommands)
        expected = CommandFailedError.command_failed("up")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, [])

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_args__run__raises_error(self, _: str, args: List[str]) -> None:
        subcommands = [mock(UpSubcommand)]
        subject = UpCommand(subcommands=subcommands)
        expected = CommandFailedError.must_run_without_args("up")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_help__run__logs_help(self) -> None:
        subject = UpCommand(subcommands=[mock(UpSubcommand), mock(UpSubcommand)])
        expected = CommandHelp(
            help="Runs a bunch of commands in order to set up the project before running it",
            summaries=[],
        )

        TestHelpCommand.assert_help_runs(subject, mock(Process, strict=True), expected)

    def _subcommand(self, i: int, has_succeeded: bool, times: int = 1) -> Any:
        subcommand = mock(UpSubcommand, strict=True)
        expect(subcommand, times=times).run(self.process).thenReturn(
            UpResult(name=f"name_{i}", has_succeeded=has_succeeded)
        )

        return subcommand

    def _subcommand_with_details(self, i: int, has_succeeded: bool, times: int = 1) -> Any:
        subcommand = mock(UpSubcommand, strict=True)
        expect(subcommand, times=times).run(self.process).thenReturn(
            UpResult(name=f"name_{i}", has_succeeded=has_succeeded, details=f"details_{i}")
        )

        return subcommand
