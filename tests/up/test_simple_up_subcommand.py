import unittest

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions
from pydantic import ValidationError

from bin.process.process import Process
from bin.up.simple_up_subcommand import SimpleUpSubcommand
from bin.up.up_subcommand import UpResult
from tests.process.factories import ProcessResultFactory
from tests.pydantic_utils import PydanticErrorUtils


class SimpleUpCommandTest(unittest.TestCase):
    NAME = "simple"
    RUN = "RUN_COMMAND"

    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

        self.subject = SimpleUpSubcommand(name=self.NAME, run_cmd=self.RUN)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_command_succeeded__run__returns_success_result(self) -> None:
        expected = UpResult.succeeded(self.NAME)
        expect(self.process, times=1).run(self.RUN).thenReturn(ProcessResultFactory.succeeded())

        actual = self.subject.run(self.process)

        self.assertEqual(expected, actual)

    def test__given_command_failed__run__returns_failed_result(self) -> None:
        expected = UpResult.failed(self.NAME)
        expect(self.process, times=1).run(self.RUN).thenReturn(ProcessResultFactory.failed())

        actual = self.subject.run(self.process)

        self.assertEqual(expected, actual)

    def test__given_invalid_args__init__raises_error(self) -> None:
        data = {"name": "", "run_cmd": "", "extra": "field"}
        expected = [
            PydanticErrorUtils.empty("name"),
            PydanticErrorUtils.empty_cmd("run_cmd"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            SimpleUpSubcommand.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
