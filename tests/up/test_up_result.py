import unittest

from bin.up.up_subcommand import UpResult
from tests.process.factories import ProcessResultFactory


class UpResultTests(unittest.TestCase):
    def test__given_failed_process_result__from_process_result__returns_failed_up_result(
        self,
    ) -> None:
        expected = UpResult(name="name", has_succeeded=False)

        actual = UpResult.from_process_result("name", ProcessResultFactory.failed())

        self.assertEqual(expected, actual)

    def test__given_successful_process_result__from_process_result__returns_successful_up_result(
        self,
    ) -> None:
        expected = UpResult(name="super name", has_succeeded=True)

        actual = UpResult.from_process_result("super name", ProcessResultFactory.succeeded())

        self.assertEqual(expected, actual)

    def test__succeeded__returns_skipped_up_result(self) -> None:
        expected = UpResult(name="yay", has_succeeded=True)

        actual = UpResult.succeeded("yay")

        self.assertEqual(expected, actual)

    def test__failed__returns_skipped_up_result(self) -> None:
        expected = UpResult(name="nay", has_succeeded=False)

        actual = UpResult.failed("nay")

        self.assertEqual(expected, actual)
