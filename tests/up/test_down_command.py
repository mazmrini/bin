import unittest

from mockito import mock, unstub, verifyNoUnwantedInteractions

from bin.commands.command_help import CommandHelp
from bin.process.process import Process
from bin.up.down_command import DownCommand
from bin.up.up_command import UpCommand
from tests.test_cmd_utils import TestHelpCommand


class DownCommandTests(unittest.TestCase):
    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__down_command_is_an_up_command(self) -> None:
        actual = DownCommand(subcommands=[])

        self.assertIsInstance(actual, UpCommand)

    def test__given_help__run__logs_help(self) -> None:
        subject = DownCommand(subcommands=[])
        expected = CommandHelp(help="Cancels what the up command sets up", summaries=[])

        TestHelpCommand.assert_help_runs(subject, mock(Process, strict=True), expected)
