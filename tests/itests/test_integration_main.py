import subprocess
import unittest
from pathlib import Path
from typing import Dict, List

from mockito import ANY, expect, spy, spy2, unstub, verify
from parameterized import parameterized

from bin.error_handler import get_error_handler
from bin.main import Main
from bin.process.io.mute_io import MuteIo
from bin.process.process import Process

VALID_TEST_CASES_DIR = Path(__file__).parent / "test_cases" / "valid"
INVALID_TEST_CASES_DIR = Path(__file__).parent / "test_cases" / "invalid"


class MainIntegrationTests(unittest.TestCase):
    def setUp(self) -> None:
        spy2(subprocess.run)
        self.error_handler = get_error_handler()

    def tearDown(self) -> None:
        unstub()

    @parameterized.expand(
        [  # type: ignore
            ("help", ["--help"], []),
            ("env", ["env"], []),
            ("env help", ["env", "-h"], []),
            ("req", ["req"], ["python3 --version"]),
            ("requirements", ["requirements"], ["python3 --version"]),
            ("req help", ["req", "-h"], []),
            ("venv activate", ["venv", "activate"], ["echo activate"]),
            ("venv remove", ["venv", "remove"], ["echo remove"]),
            ("venv help", ["venv", "--help"], []),
            (
                "up",
                ["up"],
                ["echo activate && echo up", "echo activate && echo run up"],
            ),
            ("up help", ["up", "--help"], []),
            ("down", ["down"], ["echo activate && echo run down"]),
            ("custom", ["custom"], ["echo activate && echo custom"]),
            ("custom help", ["custom", "-h"], []),
            ("inner", ["inner"], ["echo activate && echo inner"]),
            ("inner a", ["inner", "a"], ["echo activate && echo inner a"]),
            (
                "inner a with args",
                ["inner", "a", "arg", "other arg", "last"],
                ['echo activate && echo inner a arg "other arg" last'],
            ),
            ("inner b", ["inner", "b"], ["echo activate && echo inner b"]),
            ("inner b help", ["inner", "--help"], []),
            (
                "inner b alias",
                ["inner", "bb"],
                ["echo activate && echo inner b"],
            ),
            (
                "inner b final",
                ["inner", "b", "final", "arg with space"],
                ['echo activate && echo inner b final "arg with space"'],
            ),
            (
                "inner b final last alias",
                ["inner", "b", "last"],
                ["echo activate && echo inner b final"],
            ),
            (
                "inner b final with aliases",
                ["inner", "bb", "terminal"],
                ["echo activate && echo inner b final"],
            ),
            (
                "inner b final with aliases help",
                ["inner", "bb", "terminal", "-h"],
                [],
            ),
            (
                "inner b final up using aliases",
                ["inner", "bb", "last", "up"],
                ["echo activate && echo inner b final up 0", "echo activate && echo inner b final up 1"],
            ),
        ]
    )
    def test__given_valid_file__run__returns_0_and_executes_command(
        self, _: str, args: List[str], expected_cmds: List[str]
    ) -> None:
        expected_exit_code = 0
        subject = Main(self.error_handler, Process(io=MuteIo()), VALID_TEST_CASES_DIR, args)
        env = {"ONE": "env_var_1", "TWO_NICE": "env_var_2"}

        actual = subject.run()

        self.assertEqual(expected_exit_code, actual)
        for expected_cmd in expected_cmds:
            verify(subprocess).run(expected_cmd, shell=True, env=env, capture_output=ANY)

    @parameterized.expand(
        [  # type: ignore
            (
                "new_env",
                ["new_env", "hello"],
                {"ONE": "new_env_1", "TWO_NICE": "env_var_2", "NEW_ONE": "new_one_1"},
                "echo activate && echo new_env hello",
            ),
            (
                "new_env a",
                ["new_env", "a", "arg"],
                {"ONE": "new_env_1", "TWO_NICE": "env_var_2", "NEW_ONE": "new_one_1"},
                "echo activate && echo new_env a arg",
            ),
            (
                "new_env b",
                ["new_env", "b"],
                {
                    "ONE": "new_env_1",
                    "TWO_NICE": "env_var_2",
                    "NEW_ONE": "new_one_b",
                    "ONLY_B": "only_b",
                },
                "echo activate && echo new_env b",
            ),
            (
                "new_env c alias",
                ["new_env", "cc", "with space", "other arg", "last"],
                {
                    "ONE": "c_env_var_1",
                    "TWO_NICE": "c_env_var_2",
                    "NEW_ONE": "new_one_1",
                    "ONLY_C": "only_c",
                },
                'echo activate && echo new_env c "with space" "other arg" last',
            ),
        ]
    )
    def test__given_valid_file_using_env_override_commands__run__returns_0_and_executes_command(
        self, _: str, args: List[str], env: Dict[str, str], expected_cmd: str
    ) -> None:
        expected_exit_code = 0
        subject = Main(self.error_handler, Process(io=MuteIo()), VALID_TEST_CASES_DIR, args)

        actual = subject.run()

        self.assertEqual(expected_exit_code, actual)
        verify(subprocess).run(expected_cmd, shell=True, env=env, capture_output=ANY)

    @parameterized.expand(
        [  # type: ignore
            ("empty command", [], "\nerror: command not found (exit code 1)"),
            (
                "unknown top command",
                ["unknown", "command"],
                "\nerror: command not found (exit code 1)",
            ),
            (
                "unknown venv command",
                ["venv", "unknown"],
                "\nerror: command not found (exit code 1)",
            ),
            (
                "unknown custom command",
                ["sub_only", "unknown"],
                "\nerror: command not found (exit code 1)",
            ),
            (
                "req with args",
                ["req", "one", "two"],
                "\nerror: command failed (exit code 1)",
            ),
            (
                "up with args",
                ["up", "one", "two"],
                "\nerror: command failed (exit code 1)",
            ),
            (
                "down with args",
                ["down", "one", "two"],
                "\nerror: command failed (exit code 1)",
            ),
            (
                "failing command",
                ["sub_only", "failure"],
                "\nerror: command failed (exit code 1)",
            ),
        ]
    )
    def test__given_valid_file_but_invalid_command__returns_1_and_logs_expected_error(
        self, _: str, args: List[str], expected_error: str
    ) -> None:
        expected_exit_code = 1
        process = spy(Process(io=MuteIo()))
        expect(process).unmute_logs().thenReturn(process)
        expect(process).apply_env(ANY).thenReturn(process)
        subject = Main(self.error_handler, process, VALID_TEST_CASES_DIR, args)

        actual = subject.run()

        self.assertEqual(expected_exit_code, actual)
        verify(process, times=1).log_error(expected_error)

    @parameterized.expand([("help", ["--help"]), ("no args", []), ("requirements", ["req"])])  # type: ignore
    def test__given_invalid_file__run__returns_1_and_logs_file_errors(self, _: str, args: List[str]) -> None:
        expected_exit_code = 1
        process = spy(Process(io=MuteIo()))
        expect(process).unmute_logs().thenReturn(process)
        expect(process).apply_env(ANY).thenReturn(process)
        subject = Main(self.error_handler, process, INVALID_TEST_CASES_DIR, args)

        actual = subject.run()

        self.assertEqual(expected_exit_code, actual)
        verify(process, times=1).log_error("\nerror: invalid bin file (exit code 1)")
