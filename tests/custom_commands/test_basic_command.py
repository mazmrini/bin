import unittest
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions
from parameterized import parameterized
from pydantic import ValidationError

from bin.commands.command_help import CommandHelp
from bin.commands.errors import CommandFailedError
from bin.custom_commands.basic_command import BasicCommand
from bin.models.str_command import StrCommand
from bin.process.process import Process
from tests.process.factories import ProcessResultFactory
from tests.pydantic_utils import PydanticErrorUtils
from tests.test_cmd_utils import TestHelpCommand


class BasicCommandTests(unittest.TestCase):
    RUN = "RUN"

    def setUp(self) -> None:
        self.process = mock(Process, strict=True)
        self.helpable = CommandHelp.only_self("anything")
        self.subject = BasicCommand(run_cmd=self.RUN, short_description="anything", help=self.helpable)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            ("no args", []),
            ("one arg", ["one"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_successful_command__run__returns_process(self, _: str, args: List[str]) -> None:
        expect(self.process, times=1).run(self.RUN, args).thenReturn(ProcessResultFactory.succeeded())

        actual = self.subject.run(self.process, args)

        self.assertEqual(self.process, actual)

    @parameterized.expand(
        [  # type: ignore
            ("no args", []),
            ("one arg", ["one"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_failed_command__run__raises_error(self, _: str, args: List[str]) -> None:
        expect(self.process, times=1).run(self.RUN, args).thenReturn(ProcessResultFactory.failed())
        expected = CommandFailedError.command_failed(self.RUN)

        with self.assertRaises(CommandFailedError) as ctx:
            self.subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))

    def test__given_help__run__logs_help(self) -> None:
        TestHelpCommand.assert_help_runs(self.subject, self.process, self.helpable)

    def test__given_only_cmd__no_help_ctor__has_sane_help_defaults(self) -> None:
        expected = BasicCommand(
            run_cmd="i run sometimes",
            short_description="Runs i run sometimes",
            help=CommandHelp.only_self("Runs i run sometimes"),
        )

        actual = BasicCommand.no_help(StrCommand("i run sometimes"))

        self.assertEqual(expected, actual)

    def test__given_invalid_command__parse_obj__raises_error(self) -> None:
        expected = [
            PydanticErrorUtils.empty_cmd("run_cmd"),
            PydanticErrorUtils.required("short_description"),
            PydanticErrorUtils.required("help"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            BasicCommand.parse_obj({"run_cmd": ""})

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)
