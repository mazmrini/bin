import unittest
from typing import Dict

import yaml
from pydantic import BaseModel

from bin.commands.command import Command
from bin.commands.errors import CommandParseError
from bin.commands.internal.factory import InternalCommandFactory
from bin.custom_commands.basic_command import BasicCommand
from bin.custom_commands.command_tree import CommandTree
from bin.custom_commands.dtos import CustomCommandsDto
from bin.custom_commands.dtos.custom_commands_mapper import CustomCommandsMapper


class TestModel(BaseModel):
    commands: CustomCommandsDto


class CustomCommandsMapperTests(unittest.TestCase):
    def test__given_dtos_as_str__to_command_dict__returns_expected_command_tree(
        self,
    ) -> None:
        data = yaml.safe_load(self._yaml())

        actual = CustomCommandsMapper.to_command_dict(TestModel.parse_obj(data).commands)

        self.assertEqual(self._expected(), actual)

    def test__given_empty_dto__to_command_dict__returns_empty_dict(self) -> None:
        actual = CustomCommandsMapper.to_command_dict({})

        self.assertEqual({}, actual)

    def test__given_basic_cmd_conflicting_alias_with_root__to_command_dict__raises_error(
        self,
    ) -> None:
        data = """
        commands:
          one: echo one  # clash
          two:
            run: echo two
            alias: one  # clash
          three: echo three
        """
        data = yaml.safe_load(data)
        model = TestModel.parse_obj(data).commands
        expected = "conflicting subcommand names or aliases in '__root__'"

        with self.assertRaises(CommandParseError) as ctx:
            CustomCommandsMapper.to_command_dict(model)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_basic_cmd_conflicting_aliases_with_root__to_command_dict__raises_error(
        self,
    ) -> None:
        data = """
        commands:
          one: echo one
          two:
            run: echo two
            alias: fine_1
            aliases:
              - fine_2
              - three  # clash
              - fine_3
          three: echo three  # clash
        """
        data = yaml.safe_load(data)
        model = TestModel.parse_obj(data).commands
        expected = "conflicting subcommand names or aliases in '__root__'"

        with self.assertRaises(CommandParseError) as ctx:
            CustomCommandsMapper.to_command_dict(model)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_tree_cmd_conflicting_alias_with_root__to_command_dict__raises_error(
        self,
    ) -> None:
        data = """
        commands:
          one: echo one  # clash
          two:
            alias: one  # clash
            aliases:
              - fine_1
              - fine_2
            subcommands:
              sub_1: echo sub_1
          three: echo three
        """
        data = yaml.safe_load(data)
        model = TestModel.parse_obj(data).commands
        expected = "conflicting subcommand names or aliases in '__root__'"

        with self.assertRaises(CommandParseError) as ctx:
            CustomCommandsMapper.to_command_dict(model)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_tree_cmd_conflicting_aliases_with_root__to_command_dict__raises_error(
        self,
    ) -> None:
        data = """
        commands:
          one: echo one
          two:
            alias: fine_1
            aliases:
              - fine_2
              - three  # clash
              - fine_3
            subcommands:
              sub_1: echo sub_1
          three: echo three  # clash
        """
        data = yaml.safe_load(data)
        model = TestModel.parse_obj(data).commands
        expected = "conflicting subcommand names or aliases in '__root__'"

        with self.assertRaises(CommandParseError) as ctx:
            CustomCommandsMapper.to_command_dict(model)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def _yaml(self) -> str:
        return """
        commands:
          basic: run 1
          basic_duplicate: only keeps the latest one
          basic_duplicate: keeps me
          basic_explicit:
            run: run 2
            env:
              ONE: var_1
              TWO: var_2
            aliases:
              - basic_explicit_alias_1
              - basic_explicit_alias_2
          only_subcommands:
            alias: only_subcommands_alias_1
            subcommands:
              sub_1: run 3
              sub_2:
                run: run 4
                alias: sub_2_alias_1
          run_with_subcommands:
            run: run 5
            subcommands:
              sub_1: run 6
              sub_2:
                run: run 7
                alias: sub_2_alias_1
                aliases:
                  - sub_2_alias_2
              sub_3: run 8
              sub_duplicate: keep the other one
              sub_duplicate: duplicate cmd
          complex:
            run: run 9
            subcommands:
              sub_1: run 10
              sub_2:
                run: run 11
              sub_3:
                env:
                  THREE: var_3
                  FOUR: var_4
                subcommands:
                  sub_1:
                    run: run 12
                    env:
                      FIVE: var_5
                    subcommands:
                      sub_1: run 13
                      sub_2: run 14
                  sub_2: run 15
              sub_4:
                run: run 16
                alias: sub_4_alias_1
                subcommands:
                  sub_1: run 17
                  sub_2: run 18
        """

    def _expected(self) -> Dict[str, Command]:
        return {
            "basic": self._basic(),
            "basic_duplicate": self._basic_duplicate(),
            "basic_explicit": self._basic_explicit(),
            "basic_explicit_alias_1": self._basic_explicit(),
            "basic_explicit_alias_2": self._basic_explicit(),
            "only_subcommands": self._only_subcommands(),
            "only_subcommands_alias_1": self._only_subcommands(),
            "run_with_subcommands": self._run_with_subcommands(),
            "complex": self._complex(),
        }

    def _basic(self) -> Command:
        return BasicCommand.no_help("run 1")

    def _basic_duplicate(self) -> Command:
        return BasicCommand.no_help("keeps me")

    def _basic_explicit(self) -> Command:
        return InternalCommandFactory.wrap_with_env({"ONE": "var_1", "TWO": "var_2"}, BasicCommand.no_help("run 2"))

    def _only_subcommands(self) -> Command:
        return CommandTree(
            subcommands_tree={
                "sub_1": BasicCommand.no_help("run 3"),
                "sub_2": BasicCommand.no_help("run 4"),
                "sub_2_alias_1": BasicCommand.no_help("run 4"),
            }
        )

    def _run_with_subcommands(self) -> Command:
        return CommandTree(
            self_cmd=BasicCommand.no_help("run 5"),
            subcommands_tree={
                "sub_1": BasicCommand.no_help("run 6"),
                "sub_2": BasicCommand.no_help("run 7"),
                "sub_2_alias_1": BasicCommand.no_help("run 7"),
                "sub_2_alias_2": BasicCommand.no_help("run 7"),
                "sub_3": BasicCommand.no_help("run 8"),
                "sub_duplicate": BasicCommand.no_help("duplicate cmd"),
            },
        )

    def _complex(self) -> Command:
        sub_4_cmd = CommandTree(
            self_cmd=BasicCommand.no_help("run 16"),
            subcommands_tree={
                "sub_1": BasicCommand.no_help("run 17"),
                "sub_2": BasicCommand.no_help("run 18"),
            },
        )
        return CommandTree(
            self_cmd=BasicCommand.no_help("run 9"),
            subcommands_tree={
                "sub_1": BasicCommand.no_help("run 10"),
                "sub_2": BasicCommand.no_help("run 11"),
                "sub_3": InternalCommandFactory.wrap_with_env(
                    {"THREE": "var_3", "FOUR": "var_4"},
                    CommandTree(
                        subcommands_tree={
                            "sub_1": InternalCommandFactory.wrap_with_env(
                                {"FIVE": "var_5"},
                                CommandTree(
                                    self_cmd=BasicCommand.no_help("run 12"),
                                    subcommands_tree={
                                        "sub_1": BasicCommand.no_help("run 13"),
                                        "sub_2": BasicCommand.no_help("run 14"),
                                    },
                                ),
                            ),
                            "sub_2": BasicCommand.no_help("run 15"),
                        }
                    ),
                ),
                "sub_4": sub_4_cmd,
                "sub_4_alias_1": sub_4_cmd,
            },
        )
