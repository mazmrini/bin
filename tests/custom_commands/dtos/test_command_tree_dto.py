import unittest
from typing import Dict

import yaml
from parameterized import parameterized
from pydantic import ValidationError

from bin.commands.command import Command
from bin.commands.errors import CommandParseError
from bin.commands.internal.factory import InternalCommandFactory
from bin.commands.internal.log_title_command import LogTitleCommand
from bin.custom_commands.basic_command import BasicCommand
from bin.custom_commands.command_tree import CommandTree
from bin.custom_commands.dtos.command_tree_dto import CommandTreeDto
from bin.up.down_command import DownCommand
from bin.up.simple_up_subcommand import SimpleUpSubcommand
from bin.up.up_command import UpCommand
from tests.pydantic_utils import PydanticErrorUtils


class CommandTreeDtoTests(unittest.TestCase):
    ALIAS_MSG = "command aliases are clashing"

    @parameterized.expand(
        [  # type: ignore
            (
                "basic",
                """
            run: echo basic
            """,
                {"basic": BasicCommand.no_help("echo basic")},
            ),
            (
                "only_up",
                """
            up:
            - echo up
            """,
                {
                    "only_up": CommandTree(
                        subcommands_tree={
                            "up": InternalCommandFactory.with_setup(
                                [LogTitleCommand.up()], UpCommand(subcommands=[SimpleUpSubcommand.from_str("echo up")])
                            ),
                            "down": InternalCommandFactory.with_setup(
                                [LogTitleCommand.down()], DownCommand(subcommands=[])
                            ),
                        }
                    )
                },
            ),
            (
                "basic_with_alias_and_up",
                """
            run: echo basic
            alias: one
            up:
            - echo up
            """,
                {
                    "basic_with_alias_and_up": CommandTree(
                        self_cmd=BasicCommand.no_help("echo basic"),
                        subcommands_tree={
                            "up": InternalCommandFactory.with_setup(
                                [LogTitleCommand.up()], UpCommand(subcommands=[SimpleUpSubcommand.from_str("echo up")])
                            ),
                            "down": InternalCommandFactory.with_setup(
                                [LogTitleCommand.down()], DownCommand(subcommands=[])
                            ),
                        },
                    ),
                    "one": CommandTree(
                        self_cmd=BasicCommand.no_help("echo basic"),
                        subcommands_tree={
                            "up": InternalCommandFactory.with_setup(
                                [LogTitleCommand.up()], UpCommand(subcommands=[SimpleUpSubcommand.from_str("echo up")])
                            ),
                            "down": InternalCommandFactory.with_setup(
                                [LogTitleCommand.down()], DownCommand(subcommands=[])
                            ),
                        },
                    ),
                },
            ),
            (
                "basic_with_aliases",
                """
            run: echo basic
            alias: one
            aliases:
              - two
              - three
            """,
                {
                    "basic_with_aliases": BasicCommand.no_help("echo basic"),
                    "one": BasicCommand.no_help("echo basic"),
                    "two": BasicCommand.no_help("echo basic"),
                    "three": BasicCommand.no_help("echo basic"),
                },
            ),
            (
                "basic_with_env_and_up",
                """
            run: echo basic
            env:
              ONE: var_1
              TWO: var_2
            up:
            - echo up
            """,
                {
                    "basic_with_env_and_up": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1", "TWO": "var_2"},
                        CommandTree(
                            self_cmd=BasicCommand.no_help("echo basic"),
                            subcommands_tree={
                                "up": InternalCommandFactory.with_setup(
                                    [LogTitleCommand.up()],
                                    UpCommand(subcommands=[SimpleUpSubcommand.from_str("echo up")]),
                                ),
                                "down": InternalCommandFactory.with_setup(
                                    [LogTitleCommand.down()], DownCommand(subcommands=[])
                                ),
                            },
                        ),
                    )
                },
            ),
            (
                "basic_with_env_and_alias",
                """
            run: echo basic
            env:
              ONE: var_1
              TWO: var_2
            alias: one
            aliases:
              - two
            """,
                {
                    "basic_with_env_and_alias": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1", "TWO": "var_2"},
                        BasicCommand.no_help("echo basic"),
                    ),
                    "one": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1", "TWO": "var_2"},
                        BasicCommand.no_help("echo basic"),
                    ),
                    "two": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1", "TWO": "var_2"},
                        BasicCommand.no_help("echo basic"),
                    ),
                },
            ),
        ]
    )
    def test__basic_command__parse__returns_expected(self, name: str, data: str, expected: Dict[str, Command]) -> None:
        dto = yaml.safe_load(data)

        actual = CommandTreeDto.parse_obj(dto).to_command(name)

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            (
                "tree",
                """
            run: echo tree
            subcommands:
              one: echo one
              two: echo two
            """,
                {
                    "tree": CommandTree(
                        self_cmd=BasicCommand.no_help("echo tree"),
                        subcommands_tree={
                            "one": BasicCommand.no_help("echo one"),
                            "two": BasicCommand.no_help("echo two"),
                        },
                    )
                },
            ),
            (
                "tree_no_self",
                """
            subcommands:
              one:
                run: echo one
                alias: one_one
                env:
                  ONE: var_1
                  TWO: var_2
              two: echo two
            """,
                {
                    "tree_no_self": CommandTree(
                        subcommands_tree={
                            "one": InternalCommandFactory.wrap_with_env(
                                {"ONE": "var_1", "TWO": "var_2"},
                                BasicCommand.no_help("echo one"),
                            ),
                            "one_one": InternalCommandFactory.wrap_with_env(
                                {"ONE": "var_1", "TWO": "var_2"},
                                BasicCommand.no_help("echo one"),
                            ),
                            "two": BasicCommand.no_help("echo two"),
                        }
                    )
                },
            ),
            (
                "tree_with_alias",
                """
            run: echo tree
            alias: one
            aliases:
              - two
            subcommands:
              one: echo one
              two: echo two
            """,
                {
                    "tree_with_alias": CommandTree(
                        self_cmd=BasicCommand.no_help("echo tree"),
                        subcommands_tree={
                            "one": BasicCommand.no_help("echo one"),
                            "two": BasicCommand.no_help("echo two"),
                        },
                    ),
                    "one": CommandTree(
                        self_cmd=BasicCommand.no_help("echo tree"),
                        subcommands_tree={
                            "one": BasicCommand.no_help("echo one"),
                            "two": BasicCommand.no_help("echo two"),
                        },
                    ),
                    "two": CommandTree(
                        self_cmd=BasicCommand.no_help("echo tree"),
                        subcommands_tree={
                            "one": BasicCommand.no_help("echo one"),
                            "two": BasicCommand.no_help("echo two"),
                        },
                    ),
                },
            ),
            (
                "tree_with_env",
                """
            run: echo tree
            env:
              ONE: var_1
              TWO: var_2
            subcommands:
              one: echo one
              two: echo two
            """,
                {
                    "tree_with_env": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1", "TWO": "var_2"},
                        CommandTree(
                            self_cmd=BasicCommand.no_help("echo tree"),
                            subcommands_tree={
                                "one": BasicCommand.no_help("echo one"),
                                "two": BasicCommand.no_help("echo two"),
                            },
                        ),
                    )
                },
            ),
            (
                "tree_with_env_and_alias",
                """
            run: echo tree
            alias: alias
            env:
              ONE: var_1
              TWO: var_2
            subcommands:
              one: echo one
              two: echo two
            """,
                {
                    "tree_with_env_and_alias": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1", "TWO": "var_2"},
                        CommandTree(
                            self_cmd=BasicCommand.no_help("echo tree"),
                            subcommands_tree={
                                "one": BasicCommand.no_help("echo one"),
                                "two": BasicCommand.no_help("echo two"),
                            },
                        ),
                    ),
                    "alias": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1", "TWO": "var_2"},
                        CommandTree(
                            self_cmd=BasicCommand.no_help("echo tree"),
                            subcommands_tree={
                                "one": BasicCommand.no_help("echo one"),
                                "two": BasicCommand.no_help("echo two"),
                            },
                        ),
                    ),
                },
            ),
            (
                "complex_one",
                """
            run: echo tree
            alias: alias
            env:
              ONE: var_1
            subcommands:
              inner:
                subcommands:
                  one:
                    run: echo inner one
                    up:
                    - name: inner one up
                      up: echo inner one up
                      down: echo inner one down
                    env:
                      INNER_ONE: var_1
                      INNER_TWO: var_2
                    aliases:
                      - one_one
                  two:
                    run: echo inner two
                    alias: two_two
              two: echo two
            """,
                {
                    "complex_one": InternalCommandFactory.wrap_with_env(
                        {"ONE": "var_1"},
                        CommandTree(
                            self_cmd=BasicCommand.no_help("echo tree"),
                            subcommands_tree={
                                "inner": CommandTree(
                                    subcommands_tree={
                                        "one": InternalCommandFactory.wrap_with_env(
                                            {
                                                "INNER_ONE": "var_1",
                                                "INNER_TWO": "var_2",
                                            },
                                            CommandTree(
                                                self_cmd=BasicCommand.no_help("echo inner one"),
                                                subcommands_tree={
                                                    "up": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.up()],
                                                        UpCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one up"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                    "down": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.down()],
                                                        DownCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one down"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                },
                                            ),
                                        ),
                                        "one_one": InternalCommandFactory.wrap_with_env(
                                            {
                                                "INNER_ONE": "var_1",
                                                "INNER_TWO": "var_2",
                                            },
                                            CommandTree(
                                                self_cmd=BasicCommand.no_help("echo inner one"),
                                                subcommands_tree={
                                                    "up": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.up()],
                                                        UpCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one up"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                    "down": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.down()],
                                                        DownCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one down"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                },
                                            ),
                                        ),
                                        "two": BasicCommand.no_help("echo inner two"),
                                        "two_two": BasicCommand.no_help("echo inner two"),
                                    }
                                ),
                                "two": BasicCommand.no_help("echo two"),
                            },
                        ),
                    ),
                    "alias": InternalCommandFactory.wrap_with_env(  # same as the other one
                        {"ONE": "var_1"},
                        CommandTree(
                            self_cmd=BasicCommand.no_help("echo tree"),
                            subcommands_tree={
                                "inner": CommandTree(
                                    subcommands_tree={
                                        "one": InternalCommandFactory.wrap_with_env(
                                            {
                                                "INNER_ONE": "var_1",
                                                "INNER_TWO": "var_2",
                                            },
                                            CommandTree(
                                                self_cmd=BasicCommand.no_help("echo inner one"),
                                                subcommands_tree={
                                                    "up": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.up()],
                                                        UpCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one up"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                    "down": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.down()],
                                                        DownCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one down"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                },
                                            ),
                                        ),
                                        "one_one": InternalCommandFactory.wrap_with_env(
                                            {
                                                "INNER_ONE": "var_1",
                                                "INNER_TWO": "var_2",
                                            },
                                            CommandTree(
                                                self_cmd=BasicCommand.no_help("echo inner one"),
                                                subcommands_tree={
                                                    "up": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.up()],
                                                        UpCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one up"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                    "down": InternalCommandFactory.with_setup(
                                                        [LogTitleCommand.down()],
                                                        DownCommand(
                                                            subcommands=[
                                                                SimpleUpSubcommand(
                                                                    name="inner one up", run_cmd="echo inner one down"
                                                                )
                                                            ]
                                                        ),
                                                    ),
                                                },
                                            ),
                                        ),
                                        "two": BasicCommand.no_help("echo inner two"),
                                        "two_two": BasicCommand.no_help("echo inner two"),
                                    }
                                ),
                                "two": BasicCommand.no_help("echo two"),
                            },
                        ),
                    ),
                },
            ),
        ]
    )
    def test__command_tree__parse__returns_expected(self, name: str, data: str, expected: Dict[str, Command]) -> None:
        dto = yaml.safe_load(data)

        actual = CommandTreeDto.parse_obj(dto).to_command(name)

        self.assertEqual(expected, actual)

    def test__given_empty_dto__parse__raises_error(self) -> None:
        expected = [PydanticErrorUtils.custom("Command tree must have at least a runnable command", "__root__")]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj({})

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_invalid_minimal_command_tree_dto__parse__raises_error(self) -> None:
        data = """
        run: ""
        extra: field
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty_cmd("run"),
            PydanticErrorUtils.extra("extra"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)

    def test__given_invalid_command_tree_dto__parse__raises_error(self) -> None:
        data = """
        run: ""
        env:
          var:
            - should not
            - be a list
        alias: ""
        aliases:
          - ""
          - valid
        extra: field
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty_cmd("run"),
            PydanticErrorUtils.empty("alias"),
            PydanticErrorUtils.empty("aliases", 0),
            PydanticErrorUtils.extra("extra"),
            PydanticErrorUtils.custom("values must be a string or number", "env"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)

    def test__given_invalid_aliases__parse__raises_error(self) -> None:
        data = """
        run: ""
        alias: ""
        aliases:
          - one
          - ""
          - three
        subcommands:
          first: echo first
          second:
            run: echo second
            alias: match
            aliases:
              - one
              - match
              - three
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty_cmd("run"),
            PydanticErrorUtils.empty("alias"),
            PydanticErrorUtils.empty("aliases", 1),
            PydanticErrorUtils.custom(self.ALIAS_MSG, "subcommands", "second", "aliases", 1),
        ]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        for e in expected:
            self.assertIn(e, actual)

    def test__given_invalid_complex_subcommand_tree_in_tree_dto__parse__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        subcommands:
          my_cmd:
            run: valid
            alias: valid
            aliases:
              - one
              - ""
              - two
            subcommands:
              invalid_cmd: ""
              alias_cmd:
                run: ""
                alias: ""
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.empty("subcommands", "my_cmd", "aliases", 1),
            PydanticErrorUtils.empty_cmd("subcommands", "my_cmd", "subcommands", "invalid_cmd"),
            PydanticErrorUtils.empty_cmd("subcommands", "my_cmd", "subcommands", "alias_cmd", "run"),
            PydanticErrorUtils.empty("subcommands", "my_cmd", "subcommands", "alias_cmd", "alias"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        for e in expected:
            self.assertIn(e, actual)

    def test__given_alias_uses_up_or_down__to_command__raises_error(self) -> None:
        data = """
        run: running
        alias: up
        aliases:
          - valid
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.custom("up/down are reserved commands", "alias"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)

    def test__given_aliases_uses_up_or_down__to_command__raises_error(self) -> None:
        data = """
        run: running
        aliases:
          - valid
          - up
          - down
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.custom("up/down are reserved commands", "aliases", 1),
            PydanticErrorUtils.custom("up/down are reserved commands", "aliases", 2),
        ]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)

    def test__given_subcommands_uses_up_or_down__to_command__raises_error(self) -> None:
        data = """
        run: running
        subcommands:
          up: echo up
        """
        data = yaml.safe_load(data)
        expected = [
            PydanticErrorUtils.custom("up/down are reserved commands", "subcommands"),
        ]

        with self.assertRaises(ValidationError) as ctx:
            CommandTreeDto.parse_obj(data)

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertCountEqual(expected, actual)

    def test__given_alias_clashes_in_subcommands__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        subcommands:
          one:
            run: echo one
            alias: two  # clash
            aliases:
              - valid_one
              - valid_two
            subcommands:
              first: echo first
          two: echo two  # clash
          three: echo three
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'my_cmd'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_one_of_aliases_clashes_in_subcommands__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        subcommands:
          one: echo one
          two: echo two
          clash:
            run: echo clash
            subcommands:
              clash_one:
                run: something
                alias: valid
                aliases:
                  - valid_one
                  - clash_two  # clash
                  - valid_three
                subcommands:
                  first: ok
              one_ok: echo one ok
              clash_two: run me  # clash
              three_ok: echo three ok
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'clash'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_alias_clashes_with_self_subcommand__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        alias: my_cmd  # clash with self
        subcommands:
          one: echo one
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'my_cmd'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_one_aliases_clashes_with_self_subcommand__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        alias: valid
        aliases:
          - my_cmd  # clash with self
          - im_good
        subcommands:
          one: echo one
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'my_cmd'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_basic_cmd_alias_clashes_in_subcommands__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        subcommands:
          one:  # basic command
            run: echo one
            alias: two  # clash
            aliases:
              - valid_one
              - valid_two
          two: echo two  # clash
          three: echo three
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'my_cmd'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_one_of_basic_cmd_aliases_clashes_in_subcommands__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        subcommands:
          one: echo one
          two: echo two
          clash:
            run: echo clash
            subcommands:
              clash_one:  # basic command
                run: something
                alias: valid
                aliases:
                  - valid_one
                  - clash_two  # clash
                  - valid_three
              one_ok: echo one ok
              clash_two: run me  # clash
              three_ok: echo three ok
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'clash'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_no_subcommands_and_alias_clashes_with_self__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        alias: my_cmd  # clash with self
        aliases:
          - valid
          - im_good
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'my_cmd'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_no_subcommands_and_one_aliases_clashes_with_self__to_command__raises_error(
        self,
    ) -> None:
        data = """
        run: running
        alias: valid
        aliases:
          - my_cmd  # clash with self
          - im_good
        """
        data = yaml.safe_load(data)
        expected = "conflicting subcommand names or aliases in 'my_cmd'"

        with self.assertRaises(CommandParseError) as ctx:
            CommandTreeDto.parse_obj(data).to_command("my_cmd")

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)
