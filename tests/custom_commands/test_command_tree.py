import unittest
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized
from pydantic import ValidationError

from bin.commands.command import Command
from bin.commands.command_help import CommandHelp, CommandSummary
from bin.commands.errors import CommandNotFound
from bin.custom_commands.command_tree import CommandTree
from bin.process.process import Process
from tests.commands.factories import CommandTestFactory
from tests.pydantic_utils import PydanticErrorUtils
from tests.test_cmd_utils import TestHelpCommand


class CommandTreeTests(unittest.TestCase):
    def setUp(self) -> None:
        self.self_cmd = mock(Command, strict=True)
        self.process = mock(Process, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_no_self_nor_subcommands__parse__raises_error(self) -> None:
        expected = [PydanticErrorUtils.custom("command tree must have at least 1 subcommand", "__root__")]

        with self.assertRaises(ValidationError) as ctx:
            CommandTree(self_cmd=None, subcommands_tree={})

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_self_but_no_subcommands__parse__raises_error(self) -> None:
        expected = [PydanticErrorUtils.custom("command tree must have at least 1 subcommand", "__root__")]

        with self.assertRaises(ValidationError) as ctx:
            CommandTree(self_cmd=CommandTestFactory.short_desc("something"), subcommands_tree={})

        actual = PydanticErrorUtils.from_exception(ctx.exception)

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["subcommand", "arg"], ["arg"]),
            ("multiple args", ["subcommand", "multiple", "args"], ["multiple", "args"]),
        ]
    )
    def test__given_matching_subcommand_with_self_cmd__run__prioritizes_subcommand_and_runs_it_with_args(
        self, _: str, args: List[str], subcommand_args: List[str]
    ) -> None:
        subcommand = mock(Command)
        subcommand_process = mock(Process)
        expect(subcommand, times=1).run(self.process, subcommand_args).thenReturn(subcommand_process)
        subject = CommandTree(
            self_cmd=self.self_cmd,
            subcommands_tree={"not_matching": mock(Command), "subcommand": subcommand},
        )

        actual = subject.run(self.process, args)

        self.assertEqual(subcommand_process, actual)
        verifyZeroInteractions(self.self_cmd)

    @parameterized.expand(
        [  # type: ignore
            ("no args", ["subcommand"], []),
            ("one arg", ["subcommand", "arg"], ["arg"]),
            ("multiple args", ["subcommand", "multiple", "args"], ["multiple", "args"]),
        ]
    )
    def test__given_matching_subcommand_without_self_cmd__run__runs_subcommand_with_args(
        self, _: str, args: List[str], subcommand_args: List[str]
    ) -> None:
        subcommand = mock(Command)
        subcommand_process = mock(Process)
        expect(subcommand, times=1).run(self.process, subcommand_args).thenReturn(subcommand_process)
        subject = CommandTree(
            self_cmd=None,
            subcommands_tree={"not_matching": mock(Command), "subcommand": subcommand},
        )

        actual = subject.run(self.process, args)

        self.assertEqual(subcommand_process, actual)
        verifyZeroInteractions(self.self_cmd)

    def test__given_subcommands_with_self_cmd_without_args__run__runs_self_cmd(
        self,
    ) -> None:
        self_process = mock(Process)
        expect(self.self_cmd, times=1).run(self.process, []).thenReturn(self_process)
        subject = CommandTree(self_cmd=self.self_cmd, subcommands_tree={"subcommand": mock(Command)})

        actual = subject.run(self.process, [])

        self.assertEqual(self_process, actual)

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["subcommand"]),
            ("multiple args", ["subcommand", "one"]),
        ]
    )
    def test__given_no_matching_subcommands_with_self_cmd__run__runs_self_cmd_with_args(
        self, _: str, args: List[str]
    ) -> None:
        self_process = mock(Process)
        expect(self.self_cmd, times=1).run(self.process, args).thenReturn(self_process)
        subject = CommandTree(self_cmd=self.self_cmd, subcommands_tree={"not_matching": mock(Command)})

        actual = subject.run(self.process, args)

        self.assertEqual(self_process, actual)

    @parameterized.expand(
        [  # type: ignore
            ("no args", []),
            ("one arg", ["subcommand"]),
            ("multiple args", ["subcommand", "two"]),
        ]
    )
    def test__given_no_matching_subcommands_without_self_cmd__run__raises_error(self, _: str, args: List[str]) -> None:
        subject = CommandTree(self_cmd=None, subcommands_tree={"not_matching": mock(Command)})

        with self.assertRaises(CommandNotFound):
            subject.run(self.process, args)

    def test__given_complex_tree_with_self_command__run_help__logs_self_help_plus_command_short_desc(
        self,
    ) -> None:
        subject = CommandTree(
            self_cmd=CommandTestFactory.short_desc("Self help"),
            subcommands_tree={
                "first": CommandTestFactory.short_desc("first help"),
                "second": CommandTestFactory.short_desc("second help"),
                "complex_with_self": CommandTree(
                    self_cmd=CommandTestFactory.short_desc("complex self description"),
                    subcommands_tree=CommandTestFactory.useless_tree(),
                ),
                "complex_without_self": CommandTree(subcommands_tree=CommandTestFactory.useless_tree()),
                "very_long_command_that_takes_a_lot_of_place": CommandTestFactory.short_desc("very long command"),
                "last_one": CommandTestFactory.short_desc("last help"),
            },
        )
        expected = CommandHelp(
            help="Self help",
            summaries=[
                CommandSummary(command="first", help="first help"),
                CommandSummary(command="second", help="second help"),
                CommandSummary(command="complex_with_self", help="complex self description"),
                CommandSummary(
                    command="complex_without_self",
                    help="Only subcommands are available. Run them with --help to get more details",
                ),
                CommandSummary(
                    command="very_long_command_that_takes_a_lot_of_place",
                    help="very long command",
                ),
                CommandSummary(command="last_one", help="last help"),
            ],
        )

        TestHelpCommand.assert_help_runs(subject, self.process, expected)

    def test__given_complex_tree_but_no_self_command__get_help__returns_expected_command_help(
        self,
    ) -> None:
        subject = CommandTree(
            self_cmd=None,
            subcommands_tree={
                "first": CommandTestFactory.short_desc("first help"),
                "second": CommandTestFactory.short_desc("second help"),
                "complex_with_self": CommandTree(
                    self_cmd=CommandTestFactory.short_desc("complex self description"),
                    subcommands_tree=CommandTestFactory.useless_tree(),
                ),
                "complex_without_self": CommandTree(subcommands_tree=CommandTestFactory.useless_tree()),
                "very_long_command_that_takes_a_lot_of_place": CommandTestFactory.short_desc("very long command"),
                "last_one": CommandTestFactory.short_desc("last help"),
            },
        )
        expected = CommandHelp(
            help="Only subcommands are available. Run them with --help to get more details",
            summaries=[
                CommandSummary(command="first", help="first help"),
                CommandSummary(command="second", help="second help"),
                CommandSummary(command="complex_with_self", help="complex self description"),
                CommandSummary(
                    command="complex_without_self",
                    help="Only subcommands are available. Run them with --help to get more details",
                ),
                CommandSummary(
                    command="very_long_command_that_takes_a_lot_of_place",
                    help="very long command",
                ),
                CommandSummary(command="last_one", help="last help"),
            ],
        )

        TestHelpCommand.assert_help_runs(subject, self.process, expected)
