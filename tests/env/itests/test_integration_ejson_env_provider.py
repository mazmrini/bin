import unittest
from pathlib import Path

from bin.env.ejson_env_provider import EjsonEnvProvider

KEYS_DIR_PATH = Path(__file__).parent / "test_cases" / "keys"
VALID_EJSON_PATH = Path(__file__).parent / "test_cases" / "valid.ejson"
INVALID_EJSON_PATH = Path(__file__).parent / "test_cases" / "invalid.ejson"


class EjsonEnvProviderITests(unittest.TestCase):
    def setUp(self) -> None:
        self.subject = EjsonEnvProvider(["ejson", "-keydir", str(KEYS_DIR_PATH), "decrypt"])

    def test__given_no_ejson_key__merge__returns_param_env(self) -> None:
        env = {"a": 1, "b": "something"}

        actual = self.subject.merge(env)

        self.assertEqual(env, actual)

    def test__given_valid_ejson__merge__returns_expected_env(self) -> None:
        env = {
            "a": "value",
            "b": 100,
            "ejson": str(VALID_EJSON_PATH),
            "_WITH_DUNDER": "keep original one",
        }
        expected = {
            "a": "value",
            "b": 100,
            "MY_FIRST_VAR": "my first var",
            "DB_USERNAME": "dbusername",
            "DB_PASSWORD": "dbpassword123",
            "DB_PORT": 9000,
            "_WITH_DUNDER": "keep original one",
        }

        actual = self.subject.merge(env)

        self.assertEqual(expected, actual)

    def test__given_invalid_ejson__merge__raises_expected_error(self) -> None:
        env = {"a": "value", "ejson": str(INVALID_EJSON_PATH)}
        expected = "ejson file is missing the environment dict"

        with self.assertRaises(ValueError) as ctx:
            self.subject.merge(env)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)
