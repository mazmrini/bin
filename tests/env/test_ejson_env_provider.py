import subprocess
import unittest
from typing import Any, Dict

from mockito import expect, spy2, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.env.ejson_env_provider import EjsonEnvProvider
from tests.process.factories import CompletedProcessFactory


class EjsonEnvProviderTests(unittest.TestCase):
    def setUp(self) -> None:
        self.subject = EjsonEnvProvider()

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            (
                "ejson values are combined with the existing env without the ejson key",
                {"ejson": "path/to/ejson", "a": "cool", "b": 10},
                "path/to/ejson",
                b"""
                {
                  "other": 100,
                  "env":
                    {
                      "c": "very nice",
                      "A_VAR": 8000,
                      "_var_1": "var_1",
                      "__VAR_2": "var_2"
                    }
                  }
                """,
                {
                    "a": "cool",
                    "b": 10,
                    "c": "very nice",
                    "A_VAR": 8000,
                    "var_1": "var_1",
                    "_VAR_2": "var_2",
                },
            ),
            (
                "conflicting env values prioritizes parameter env without the ejson key",
                {"ejson": "path/to/ejson", "a": "cool", "b": 10, "_VAR_1": "original"},
                "path/to/ejson",
                b"""
                {
                  "other": 100,
                  "env": {
                    "a": "is ignored",
                    "c": "not so fast",
                    "__VAR_1": "overwritten",
                    "_VAR_2": "kept"
                  }
                }""",
                {
                    "a": "cool",
                    "b": 10,
                    "c": "not so fast",
                    "_VAR_1": "original",
                    "VAR_2": "kept",
                },
            ),
        ]
    )
    def test__given_ejson_in_env_and_file_exists__merge__returns_merged_ejson_env_with_parameter(
        self,
        _: str,
        env_param: Dict[Any, Any],
        ejson_path: str,
        ejson_env: bytes,
        expected: Dict[Any, Any],
    ):
        expect(subprocess, times=1).run(["ejson", "decrypt", ejson_path], capture_output=True).thenReturn(
            CompletedProcessFactory.success(ejson_env)
        )

        actual = self.subject.merge(env_param)

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            ("empty", {}),
            ("with content", {"a": 1, "b": "something"}),
            (
                "ejson but not lowercase",
                {
                    "a": 1,
                    "EJSON": "nice try",
                    "eJson": "sad days",
                    "Ejson": "still not it",
                },
            ),
        ]
    )
    def test__given_ejson_not_in_env__merge__returns_input_env(self, _: str, env: Dict[Any, Any]) -> None:
        spy2(subprocess.run)

        actual = self.subject.merge(env)

        self.assertEqual(env, actual)
        verifyZeroInteractions(subprocess)

    @parameterized.expand(
        [  # type: ignore
            (
                "empty ejson file content",
                {"ejson": "path/to/ejson", "a": "cool", "b": 10},
                "path/to/ejson",
                b"{}",
            ),
            (
                "contains other keys but not the environment one",
                {"ejson": "path/to/ejson", "a": "cool", "b": 10},
                "path/to/ejson",
                b"""{"other": 100, "useless": [1, 2, 3], "envs": {"not it": "sadly"}}""",
            ),
        ]
    )
    def test__given_ejson_in_env_but_file_has_no_env__merge__raises_value_error(
        self, _: str, env_param: Dict[Any, Any], ejson_path: str, ejson_env: bytes
    ):
        expected = "ejson file is missing the environment dict"
        expect(subprocess, times=1).run(["ejson", "decrypt", ejson_path], capture_output=True).thenReturn(
            CompletedProcessFactory.success(ejson_env)
        )

        with self.assertRaises(ValueError) as ctx:
            self.subject.merge(env_param)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            ("ejson value is a list", {"ejson": ["something", 10], "a": 20}),
            ("ejson value is a dict", {"ejson": {"something": 10}, "other": "value"}),
            ("ejson value is an int", {"ejson": 300, "other": "value"}),
            ("ejson value is a float", {"ejson": 10.33, "other": "value"}),
        ]
    )
    def test__given_ejson_value_is_invalid__merge__raises_value_error(self, _: str, env_param: Dict[Any, Any]) -> None:
        expected = "ejson path must be a string"

        with self.assertRaises(ValueError) as ctx:
            self.subject.merge(env_param)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_ejson_command_fails__merge__raises_value_error(self) -> None:
        env = {"a": "value", "ejson": "invalid/file"}
        expected = "ejson decryption failed"
        expect(subprocess, times=1).run(["ejson", "decrypt", "invalid/file"], capture_output=True).thenReturn(
            CompletedProcessFactory.failure()
        )

        with self.assertRaises(ValueError) as ctx:
            self.subject.merge(env)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_ejson_command_returns_invalid_json_string__merge__raises_value_error(
        self,
    ) -> None:
        env = {"a": "value", "ejson": "path/to/ejson"}
        expected = "ejson parsing failed"
        expect(subprocess, times=1).run(["ejson", "decrypt", "path/to/ejson"], capture_output=True).thenReturn(
            CompletedProcessFactory.success(b"""not a dict, wont parse""")
        )

        with self.assertRaises(ValueError) as ctx:
            self.subject.merge(env)

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)
