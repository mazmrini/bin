import unittest
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized

from bin.commands.errors import CommandFailedError
from bin.env.env import Env
from bin.env.log_env_command import LogEnvCommand
from bin.process.emoji import Emoji
from bin.process.process import Process
from tests.test_cmd_utils import TestHelpCommand


class LogEnvCommandTests(unittest.TestCase):
    def setUp(self) -> None:
        self.process = mock(Process, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    def test__given_env_dict__run__logs_env_var_in_alphabetical_order(self) -> None:
        env = Env(
            {
                "FIRST_VAR": "almost last",
                "b": "second",
                "A": "first",
                "Z_VAR": "surely last",
            }
        )
        subject = LogEnvCommand(env=env)
        expect(self.process, times=1).log("A=first\n" "b=second\n" "FIRST_VAR=almost last\n" "Z_VAR=surely last")

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    def test__given_empty_env__run__logs_empty_env_info(self) -> None:
        subject = LogEnvCommand(env=Env({}))
        expect(self.process, times=1).log(f"{Emoji.THINKING_FACE}  no environment variables")

        actual = subject.run(self.process, [])

        self.assertEqual(self.process, actual)

    @parameterized.expand(
        [  # type: ignore
            ("one arg", ["one arg"]),
            ("multiple args", ["one", "two", "three"]),
        ]
    )
    def test__given_args__run__raises_error(self, _: str, args: List[str]) -> None:
        subject = LogEnvCommand(env=Env({}))
        expected = CommandFailedError.must_run_without_args("env")

        with self.assertRaises(CommandFailedError) as ctx:
            subject.run(self.process, args)

        actual = ctx.exception

        self.assertEqual(str(expected), str(actual))
        verifyZeroInteractions(self.process)

    def test__given_help__run__logs_help(self) -> None:
        subject = LogEnvCommand(env=Env({}))

        TestHelpCommand.assert_help_runs(subject, self.process)
