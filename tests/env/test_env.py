import unittest
from typing import Any, Dict

import yaml
from mockito import expect, mock, unstub, verifyNoUnwantedInteractions, verifyZeroInteractions
from parameterized import parameterized
from pydantic import BaseModel, ValidationError

from bin.env.env import Env
from bin.env.env_provider import EnvProvider
from bin.env.env_providers import EnvProviders
from bin.env.errors import EnvProvisionError


class TestModel(BaseModel):
    env: Env


class EnvTests(unittest.TestCase):
    def setUp(self) -> None:
        self.env_provider = mock(EnvProvider, strict=True)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            (
                "only str",
                """
            env:
              a: hello with spaces
              b: bye,with,commas
              c: usual string
            """,
                {"a": "hello with spaces", "b": "bye,with,commas", "c": "usual string"},
            ),
            (
                "numbers are converted to strings",
                """
            env:
              a: 10
              b: 990.239
            """,
                {"a": "10", "b": "990.239"},
            ),
            (
                "mixed values",
                """
            env:
              FIRST_ENV_VAR: 10
              _third: var_3
              _1_ok: 99.12
              _100: spaces here
              ___: works
            """,
                {
                    "FIRST_ENV_VAR": "10",
                    "_third": "var_3",
                    "_1_ok": "99.12",
                    "_100": "spaces here",
                    "___": "works",
                },
            ),
        ]
    )
    def test__given_valid_dict__parse__returns_expected(self, _: str, yaml_data: str, expected: Dict[str, Any]) -> None:
        data = yaml.safe_load(yaml_data)

        actual = TestModel.parse_obj(data).env

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            (
                "only numbers",
                """
            env:
              ONE: valid
              100_200: invalid
            """,
            ),
            (
                "starts with number",
                """
            env:
              ONE: valid
              1_hello_50: invalid
            """,
            ),
            (
                "contains invalid - char",
                """
            env:
              ONE: valid
              hello-var: invalid
            """,
            ),
            (
                "contains invalid = char",
                """
            env:
              ONE: valid
              hello=var: invalid
            """,
            ),
            (
                "contains invalid = char",
                """
            env:
              ONE: valid
              "": invalid
            """,
            ),
        ]
    )
    def test__given_invalid_keys_dict__parse__raises_expected_error(self, _: str, yaml_data: str) -> None:
        expected = "keys must contain only alphanumerical or underscore chars"
        data = yaml.safe_load(yaml_data)

        with self.assertRaises(ValidationError) as ctx:
            TestModel.parse_obj(data)

        actual = ctx.exception.errors()[0]["msg"]

        self.assertEqual(expected, actual)

    @parameterized.expand(
        [  # type: ignore
            (
                "list is an invalid value",
                """
            env:
              ONE: valid
              TWO:
                - invalid list
            """,
            ),
            (
                "dict is invalid value",
                """
            env:
              ONE: valid
              TWO:
                a: dict
                b: value
            """,
            ),
        ]
    )
    def test__given_invalid_values__parse__raises_expected_error(self, _: str, yaml_data: str) -> None:
        expected = "values must be a string or number"
        data = yaml.safe_load(yaml_data)

        with self.assertRaises(ValidationError) as ctx:
            TestModel.parse_obj(data)

        actual = ctx.exception.errors()[0]["msg"]

        self.assertEqual(expected, actual)
        verifyZeroInteractions(self.env_provider)

    @parameterized.expand(
        [  # type: ignore
            (
                "a list",
                """
            env:
              - one
              - two
            """,
            ),
            (
                "a string",
                """
            env: hello
            """,
            ),
            (
                "an int",
                """
            env: 20291
            """,
            ),
            (
                "a float",
                """
            env: 5.32
            """,
            ),
        ]
    )
    def test__given_anything_but_a_dict__parse__raises_expected_error(self, _: str, yaml_data: str) -> None:
        expected = "must be a dictionary"
        data = yaml.safe_load(yaml_data)

        with self.assertRaises(ValidationError) as ctx:
            TestModel.parse_obj(data)

        actual = ctx.exception.errors()[0]["msg"]

        self.assertEqual(expected, actual)

    def test__given_valid_env_with_providers__provide__returns_providers_env(
        self,
    ) -> None:
        subject = Env({"a": "1", "ejson": "path"})
        provided = {"a": 11, "b": 20, "e": 3}
        expected = {"a": "11", "b": "20", "e": "3"}
        expect(EnvProviders, times=1).init().thenReturn(self.env_provider)
        expect(self.env_provider).merge(subject).thenReturn(provided)

        actual = subject.provide()

        self.assertEqual(expected, actual)

    def test__given_invalid_providers_env__provide__raises_env_provision_error(
        self,
    ) -> None:
        subject = Env({"a": "hello"})
        provided = {"a": "hello", "1_invalid_key": "should fail"}
        expect(EnvProviders, times=1).init().thenReturn(self.env_provider)
        expect(self.env_provider).merge(subject).thenReturn(provided)
        expected = str(EnvProvider.key_error())

        with self.assertRaises(EnvProvisionError) as ctx:
            subject.provide()

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)

    def test__given_provider_raises_value_error__provide__raises_env_provision_error(
        self,
    ) -> None:
        subject = Env({"a": "will fail"})
        expected = "im custom error"
        expect(EnvProviders, times=1).init().thenReturn(self.env_provider)
        expect(self.env_provider).merge(subject).thenRaise(EnvProvider.custom_error(expected))

        with self.assertRaises(EnvProvisionError) as ctx:
            subject.provide()

        actual = str(ctx.exception)

        self.assertEqual(expected, actual)
