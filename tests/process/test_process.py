import shutil
import subprocess
import unittest
from subprocess import CompletedProcess
from typing import List

from mockito import expect, mock, unstub, verifyNoUnwantedInteractions
from parameterized import parameterized

from bin.process.helpable import Helpable
from bin.process.io.io import Io
from bin.process.process import Process
from bin.process.process_result import ProcessResult
from tests.process.factories import CompletedProcessFactory, ProcessResultFactory


class ProcessTestData:
    def __init__(self, completed_process: CompletedProcess[bytes], process_result: ProcessResult) -> None:
        self.completed_process = completed_process
        self.process_result = process_result

    @classmethod
    def success(cls, stdout: bytes = b"") -> "ProcessTestData":
        return cls(
            CompletedProcessFactory.success(stdout),
            ProcessResultFactory.succeeded(stdout.decode()),
        )

    @classmethod
    def failure(cls, stderr: bytes = b"") -> "ProcessTestData":
        return cls(
            CompletedProcessFactory.failure(stderr),
            ProcessResultFactory.failed(stderr.decode()),
        )


class ProcessTests(unittest.TestCase):
    def setUp(self) -> None:
        self.io = mock(Io, strict=True)
        self.subject = Process(io=self.io)

    def tearDown(self) -> None:
        verifyNoUnwantedInteractions()
        unstub()

    @parameterized.expand(
        [  # type: ignore
            ("no args", "echo", [], True, "echo"),
            ("one arg", "echo", ["one"], False, "echo one"),
            ("one arg", "echo", ["with space"], True, 'echo "with space"'),
            ("multiple args", "echo", ["one", "two"], False, "echo one two"),
            (
                "multiple args",
                "echo",
                ["one", "with space", "me too"],
                False,
                'echo one "with space" "me too"',
            ),
        ]
    )
    def test__given_default_process__run__runs_the_command_with_default_values(
        self, _: str, cmd: str, args: List[str], capture_output: bool, expected_cmd: str
    ) -> None:
        test_data = ProcessTestData.success(b"result")
        expect(subprocess, times=1).run(expected_cmd, shell=True, env={}, capture_output=capture_output).thenReturn(
            test_data.completed_process
        )

        actual = self.subject.run(cmd, args, capture_output=capture_output)

        self.assertEqual(test_data.process_result, actual)

    @parameterized.expand(
        [  # type: ignore
            ("no args", "echo", [], True, "venv && echo"),
            ("one arg", "echo", ["one"], True, "venv && echo one"),
            ("one arg", "echo", ["with space"], False, 'venv && echo "with space"'),
            ("multiple args", "echo", ["one", "two"], True, "venv && echo one two"),
            (
                "multiple args",
                "echo",
                ["one", "with space", "me too"],
                False,
                'venv && echo one "with space" "me too"',
            ),
        ]
    )
    def test__given_venv_process__run__runs_venv_command_first(
        self, _: str, cmd: str, args: List[str], capture_output: bool, expected_cmd: str
    ) -> None:
        test_data = ProcessTestData.success(b"something")
        expect(subprocess, times=2).run(expected_cmd, shell=True, env={}, capture_output=capture_output).thenReturn(
            test_data.completed_process
        )

        actual = Process(io=self.io, venv="nope").with_venv("venv").run(cmd, args, capture_output=capture_output)
        with_venv_actual = self.subject.with_venv("venv").run(cmd, args, capture_output=capture_output)

        self.assertEqual(test_data.process_result, actual)
        self.assertEqual(test_data.process_result, with_venv_actual)

    def test__given_env_in_process__run__runs_with_environment(self) -> None:
        test_data = ProcessTestData.failure(b"i failed")
        env = {"a": "value", "b": "other"}
        applied_env = {"b": "overwrite", "c": "new"}
        expected_env = {"a": "value", "b": "overwrite", "c": "new"}
        expect(subprocess, times=2).run("command", shell=True, env=expected_env, capture_output=False).thenReturn(
            test_data.completed_process
        )

        actual = Process(io=self.io, env=env).apply_env(applied_env).run("command")
        apply_env_actual = self.subject.apply_env(expected_env).run("command")

        self.assertEqual(test_data.process_result, actual)
        self.assertEqual(test_data.process_result, apply_env_actual)

    def test__given_multiple_options__run__runs_with_expected_args(self) -> None:
        test_data = ProcessTestData.success(b"failed")
        env = {"i exist": "in env"}
        venv = "venv command"
        expect(subprocess, times=4).run(
            "venv command && real command", shell=True, env=env, capture_output=False
        ).thenReturn(test_data.completed_process)

        actual_1 = Process(io=self.io, env=env).with_venv(venv).run("real command")
        actual_2 = Process(io=self.io, venv=venv).apply_env(env).run("real command")
        actual_3 = self.subject.with_venv(venv).apply_env(env).run("real command")
        actual_4 = (
            Process(io=self.io, venv="will be overwritten", env={"i exist": "wrong value"})
            .with_venv(venv)
            .apply_env(env)
            .run("real command")
        )

        self.assertEqual(test_data.process_result, actual_1)
        self.assertEqual(test_data.process_result, actual_2)
        self.assertEqual(test_data.process_result, actual_3)
        self.assertEqual(test_data.process_result, actual_4)

    def test__given_helpable__log_help__writes_to_io_helpable_formatted_message(
        self,
    ) -> None:
        term_cols = 2931
        expect(shutil, times=1).get_terminal_size().thenReturn((term_cols, 100))
        helpable = mock(Helpable, strict=True)
        expect(helpable, times=1).format_help(term_cols).thenReturn("Im the help")
        expect(self.io, times=1).text("Im the help")

        self.subject.log_help(helpable)
