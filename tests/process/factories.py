from subprocess import CompletedProcess

from bin.process.process_result import ProcessResult


class ProcessResultFactory:
    @staticmethod
    def succeeded(stdout: str = "") -> ProcessResult:
        return ProcessResult(exit_code=0, stdout=stdout)

    @staticmethod
    def failed(stderr: str = "") -> ProcessResult:
        return ProcessResult(exit_code=1, stderr=stderr)


class CompletedProcessFactory:
    @staticmethod
    def success(stdout: bytes = b"") -> CompletedProcess:  # type: ignore
        return CompletedProcess("args", 0, stdout=stdout)

    @staticmethod
    def failure(stderr: bytes = b"") -> CompletedProcess:  # type: ignore
        return CompletedProcess("args", 1, stderr=stderr)
